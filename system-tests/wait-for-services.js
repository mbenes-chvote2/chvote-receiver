/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

let http = require('http');
let requestEvery = 5000;
let globalTimeOut = 20 * 60 * 1000;
let startTime = new Date().getTime();

let serviceUp = (resp) => {
    try {
        return JSON.parse(resp).status === "UP"
    } catch (e) {
        return false;
    }
};

let servicesToTest = [
    {
        host: 'docker',
        path: '/health/mock-server',
        port: '80',
        isOk: serviceUp,
        name: "mock server"
    },
    {
        host: 'docker',
        path: '/health/backend',
        port: '80',
        isOk: serviceUp,
        name: "backend"
    }
];

servicesToTest.forEach((service) => service.status = "nok");

let testServices = () => {
    servicesToTest.filter(s => s.status === "nok")
        .forEach(s => {
            s.status = "pending";
            let callback = function (response) {
                let str = '';
                response.on('data', function (chunk) {
                    str += chunk;
                });

                response.on('end', function () {
                    if (s.isOk(str)) {
                        s.status = "ok";
                        console.log("service " + s.name + " is up");
                    } else {
                        s.status = "nok";
                        console.log("service " + s.name + " is still down. Retry in : " + requestEvery / 1000 + "s");
                    }
                });
            };
            let req = http.request({host: s.host, path: s.path, port: s.port}, callback);
            req.on('error', function (error) {
                s.status = "nok";
                console.log("service " + s.name + " is still down. Retry in : " + requestEvery / 1000 + "s");
            });
            req.end();
        });

    if (startTime + globalTimeOut < new Date().getTime()) {
        throw new Error("Global Timeout. Service(s): " +
            servicesToTest.filter(s => s.status !== "ok").map(s => s.name).join(", ") + " is/are not started");

    } else if (servicesToTest.filter(s => s.status !== "ok").length > 0) {
        setTimeout(testServices, requestEvery);
    }
};
console.log("Testing services: " + servicesToTest.map(s => s.name).join(", "));
testServices();

/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { createUseCase, setUserStatusInVrum } from '../shared/mock-server';
import { browser, ExpectedConditions } from 'protractor';
import { Main } from '../page-objects/main.po';
import { IdentificationPage } from '../page-objects/identification.po';
import { BallotPage } from '../page-objects/ballot.po';
import { SummaryPage } from '../page-objects/summary.po';
import { ChvoteDialog } from '../page-objects/chvote-dialog.po';

describe('Verification code revealed', () => {

  let protocolInstanceId;
  let voterId;
  let voterIdAndCardNumber;
  let confirmationCode;
  let verificationCodes;
  let finalizationCode;

  beforeAll((done) => {
    browser.get('/test/vote/');
    browser.controlFlow().execute(
      () => createUseCase()
        .then(useCaseMetaData => {
            protocolInstanceId = useCaseMetaData.protocolInstanceId;
            voterId = useCaseMetaData.votingCards[0].voterId;
            voterIdAndCardNumber = `${voterId}:${useCaseMetaData.votingCards[0].cardNumber}`;
            confirmationCode = useCaseMetaData.votingCards[0].confirmationCode;
            verificationCodes = useCaseMetaData.votingCards[0].verificationCodes;
            finalizationCode = useCaseMetaData.votingCards[0].finalizationCode;
          }
        )
        .then(done)
    );
  });

  it('page title should be "ÉTAPE 1 : IDENTIFICATION"', () => {
    Main.navigateTo(`/test/vote/ge/${protocolInstanceId}/fr/identification`);
    expect(Main.stepTitleText).toBe('ÉTAPE 1 : IDENTIFICATION');
  });

  it('should accept to identify the voter if VRUM accept it', () => {
    browser.controlFlow().execute(() =>
      setUserStatusInVrum(protocolInstanceId, voterId, 'READY_TO_VOTE')
    );
    IdentificationPage.cardNumberInput.value = voterIdAndCardNumber;
    Main.nextStepButton.click();
    expect(Main.stepTitle.isDisplayed());
  });


  it('Legal Information continue button should navigate to the ballot page', () => {
    Main.nextStepButton.click();
    expect(Main.stepTitle.isDisplayed());
  });

  it('Ballot continue button should navigate to summary page', () => {
    BallotPage.getCheckbox(1).click();
    BallotPage.getCheckbox(4).click();
    BallotPage.getCheckbox(7).click();
    BallotPage.getCheckbox(13).click();
    BallotPage.getCheckbox(17).click();
    Main.nextStepButton.click();
    expect(Main.stepTitle.isDisplayed());
  });

  it('Should validate summary', () => {
    SummaryPage.acceptCheckbox.click();
    SummaryPage.birthDateInput.value = '25/08/1980';
    Main.nextStepButton.click();
    expect(Main.stepTitle.isDisplayed());
  });

  it("should prevent user to do identification since verification codes has been revealed", () => {
    Main.navigateTo(`/test/vote/ge/${protocolInstanceId}/fr/identification`);
    IdentificationPage.cardNumberInput.value = voterIdAndCardNumber;
    Main.nextStepButton.click();
    browser.wait(ExpectedConditions.visibilityOf(ChvoteDialog.dialog));
    expect(ChvoteDialog.message).toEqual(
      "Vos codes de vérification ont déjà été révélés lors d'une tentative de vote précédente. " +
      "Or, vous n'avez pas confirmé votre vote à l'aide du code de confirmation. Par mesure de sécurité, " +
      "il ne vous est plus possible de voter par internet. " +
      "Vous pouvez par contre toujours voter par un autre canal (correspondance ou au local)."
    );
    ChvoteDialog.clickPrimary();
  });


});


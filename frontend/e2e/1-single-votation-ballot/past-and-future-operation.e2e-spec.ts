/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { Canton, createUseCase, DateType, TestCaseType } from '../shared/mock-server';
import { browser } from 'protractor';
import { Main } from '../page-objects/main.po';

describe('Past operation', () => {

  let protocolInstanceId;

  beforeAll((done) => {
    browser.get('/test/vote/');
    browser.controlFlow().execute(
      () => createUseCase(false, true, TestCaseType.SIMULATION, Canton.GE, true, DateType.PAST)
        .then(useCaseMetaData => {
            protocolInstanceId = useCaseMetaData.protocolInstanceId;
          }
        )
        .then(done)
    );
  });

  it('page should display an error', () => {
    Main.navigateTo(`/test/vote/ge/${protocolInstanceId}/fr/identification`);

    expect(Main.globalError.isDisplayed).toBeTruthy()
    expect(Main.globalError.getText()).toEqual(
      "La période de vote par internet est terminée.\nLe vote par internet était ouvert du 08.08.2005 à 02:05 au 08.08.2006 à 03:05 (heure suisse).")
  });

});


describe('future operation', () => {

  let protocolInstanceId;

  beforeAll((done) => {
    browser.get('/test/vote/');
    browser.controlFlow().execute(
      () => createUseCase(false, true, TestCaseType.SIMULATION, Canton.GE, true, DateType.FUTURE)
        .then(useCaseMetaData => {
            protocolInstanceId = useCaseMetaData.protocolInstanceId;
          }
        )
        .then(done)
    );
  });

  it('page should display an error', () => {
    Main.navigateTo(`/test/vote/ge/${protocolInstanceId}/fr/identification`);
    browser.sleep(500)
    expect(Main.globalError.getText()).toEqual(
      "Le vote par internet n'est pas encore ouvert.\nLe vote par internet est ouvert du 10.10.2045 à 11:05 au 11.11.2046 à 12:05 (heure suisse).")
  });

});


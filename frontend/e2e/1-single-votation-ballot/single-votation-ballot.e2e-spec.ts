/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { burnVotingCard, createUseCase, setUserStatusInVrum } from '../shared/mock-server';
import { browser, ExpectedConditions } from 'protractor';
import { Main } from '../page-objects/main.po';
import { IdentificationPage } from '../page-objects/identification.po';
import { BallotPage } from '../page-objects/ballot.po';
import { SummaryPage } from '../page-objects/summary.po';
import { ControlPage } from '../page-objects/control.po';
import { FinalizationPage } from '../page-objects/finalization.po';
import { ChvoteDialog } from '../page-objects/chvote-dialog.po';

describe('Single Votation Ballot', () => {

  let protocolInstanceId;
  let voterId;
  let voterIdAndCardNumber;
  let confirmationCode;
  let verificationCodes;
  let finalizationCode;

  describe('Identification', () => {

    beforeAll((done) => {
      browser.get('/test/vote/');
      browser.controlFlow().execute(
        () => createUseCase()
          .then(useCaseMetaData => {
              protocolInstanceId = useCaseMetaData.protocolInstanceId;
              voterId = useCaseMetaData.votingCards[0].voterId;
              voterIdAndCardNumber = `${voterId}:${useCaseMetaData.votingCards[0].cardNumber}`;
              confirmationCode = useCaseMetaData.votingCards[0].confirmationCode;
              verificationCodes = useCaseMetaData.votingCards[0].verificationCodes;
              finalizationCode = useCaseMetaData.votingCards[0].finalizationCode;
            }
          )
          .then(done)
      );
    });

    it('page title should be "ÉTAPE 1 : IDENTIFICATION"', () => {
      Main.navigateTo(`/test/vote/ge/${protocolInstanceId}/fr/identification`);
      expect(Main.stepTitleText).toBe('ÉTAPE 1 : IDENTIFICATION');
    });

    Main.checkAccessibility();

    it('should refuse to identify if the card number is empty', () => {
      IdentificationPage.cardNumberInput.value = '';
      Main.nextStepButton.click();
      expect(IdentificationPage.cardNumberInput.errorIsPresent).toBeTruthy('Error should be displayed');
      expect(IdentificationPage.cardNumberInput.error).toBe('Champ obligatoire');
    });


    it('should refuse to identify the voter if the voter has already done too much authentication', () => {
      browser.controlFlow().execute(() =>
        burnVotingCard(protocolInstanceId, voterId, 'AUTHENTICATION')
      );

      IdentificationPage.cardNumberInput.value = voterIdAndCardNumber;
      Main.nextStepButton.click();

      browser.wait(ExpectedConditions.visibilityOf(ChvoteDialog.dialog));
      expect(ChvoteDialog.message).toEqual(
        "Votre authentification a échoué 5 fois. Vous ne pouvez plus voter par internet, " +
        "mais par correspondance ou à l'urne selon votre lieu de résidence."
      );
      ChvoteDialog.clickPrimary();
    });

    it('should refuse to identify the voter if the voter has already done too much confirmation', () => {
      browser.controlFlow().execute(() =>
        burnVotingCard(protocolInstanceId, voterId, 'CONFIRMATION')
      );

      IdentificationPage.cardNumberInput.value = voterIdAndCardNumber;
      Main.nextStepButton.click();

      browser.wait(ExpectedConditions.visibilityOf(ChvoteDialog.dialog));
      expect(ChvoteDialog.message).toEqual(
        "Votre confirmation a échoué 5 fois. Vous ne pouvez plus voter par internet, " +
        "mais par correspondance ou à l'urne selon votre lieu de résidence."
      );
      ChvoteDialog.clickPrimary();

      browser.controlFlow().execute(() =>
        burnVotingCard(protocolInstanceId, voterId, 'NONE')
      );
    });


    it('should refuse to identify the voter if the vote has already been done', () => {
      browser.controlFlow().execute(() =>
        setUserStatusInVrum(protocolInstanceId, voterId, 'ALREADY_VOTED')
      );

      IdentificationPage.cardNumberInput.value = voterIdAndCardNumber;
      Main.nextStepButton.click();

      browser.wait(ExpectedConditions.visibilityOf(ChvoteDialog.dialog));
      expect(ChvoteDialog.message).toEqual(
        "Vous avez déjà participé à cette opération"
      );
      ChvoteDialog.clickPrimary();
    });


    it('should accept to identify the voter if VRUM accept it', () => {
      browser.controlFlow().execute(() =>
        setUserStatusInVrum(protocolInstanceId, voterId, 'READY_TO_VOTE')
      );
      IdentificationPage.cardNumberInput.value = voterIdAndCardNumber;
      Main.nextStepButton.click();
      expect(Main.stepTitle.isDisplayed());
    });

  });

  describe('Legal Information', () => {

    it('page title should be "ÉTAPE 2 : RAPPEL LÉGAL"', () => {
      expect(Main.stepTitleText).toBe('ÉTAPE 2 : RAPPEL LÉGAL');
    });

    Main.checkAccessibility();

    it('continue button should navigate to the ballot page', () => {
      Main.nextStepButton.click();
      expect(Main.stepTitle.isDisplayed());
    });
  });

  describe('Ballot', () => {

    it('page title should be "ÉTAPE 3 : BULLETIN DE VOTE"', () => {
      expect(Main.stepTitleText).toBe('ÉTAPE 3 : BULLETIN DE VOTE');
    });

    it('all answers should be blank', () => {
      expect(BallotPage.getCheckbox(1).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(2).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(4).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(5).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(7).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(8).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(10).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(11).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(13).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(14).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(16).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(17).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(19).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(20).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(22).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(23).checked).toBeFalsy();
    });

    Main.checkAccessibility('heading-order');

    it('erase button should display a warning message', () => {
      // given
      BallotPage.getCheckbox(1).click();
      BallotPage.getCheckbox(4).click();
      BallotPage.getCheckbox(7).click();
      BallotPage.getCheckbox(13).click();
      BallotPage.getCheckbox(17).click();
      // when
      BallotPage.eraseButton.click();
      // then
      ChvoteDialog.waitForMessage();
      expect(ChvoteDialog.message)
        .toBe('Cette action annulera toutes les modifications effectuées sur la page, souhaitez-vous continuer ?');
    });

    it('choosing "No" should have no effect', () => {
      ChvoteDialog.clickSecondary();
      expect(BallotPage.getCheckbox(1).checked).toBeTruthy();
      expect(BallotPage.getCheckbox(2).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(4).checked).toBeTruthy();
      expect(BallotPage.getCheckbox(5).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(7).checked).toBeTruthy();
      expect(BallotPage.getCheckbox(8).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(10).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(11).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(13).checked).toBeTruthy();
      expect(BallotPage.getCheckbox(14).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(16).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(17).checked).toBeTruthy();
      expect(BallotPage.getCheckbox(19).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(20).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(22).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(23).checked).toBeFalsy();
    });

    it('choosing "Yes" should clean the selection', () => {
      // when
      BallotPage.eraseButton.click();
      ChvoteDialog.clickPrimary();
      // then
      expect(BallotPage.getCheckbox(1).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(2).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(4).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(5).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(7).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(8).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(10).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(11).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(13).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(14).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(16).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(17).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(19).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(20).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(22).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(23).checked).toBeFalsy();
    });

    it('continue button label should be "CONTINUER"', () => {
      expect(Main.nextStepButton.text).toBe('CONTINUER');
    });

    it('continue button should navigate to summary page', () => {
      BallotPage.getCheckbox(1).click();
      BallotPage.getCheckbox(4).click();
      BallotPage.getCheckbox(7).click();
      BallotPage.getCheckbox(13).click();
      BallotPage.getCheckbox(17).click();
      Main.nextStepButton.click();
      expect(Main.stepTitle.isDisplayed());
    });
  });

  describe('Summary', () => {

    it('page title should be "ÉTAPE 4 : RÉCAPITULATIF"', () => {
      expect(Main.stepTitleText).toBe('ÉTAPE 4 : RÉCAPITULATIF');
    });

    Main.checkAccessibility();

    it('user choices should be displayed', () => {
      expect(SummaryPage.getAnswer('1-1')).toBe('OUI');
      expect(SummaryPage.getAnswer('1-2')).toBe('OUI');
      expect(SummaryPage.getAnswer('1-3')).toBe('IN');
      expect(SummaryPage.getAnswer('1-4')).toBe('BLANC');
      expect(SummaryPage.getAnswer('2-1')).toBe('OUI');
      expect(SummaryPage.getAnswer('2-2')).toBe('NON');
      expect(SummaryPage.getAnswer('3-1')).toBe('BLANC');
      expect(SummaryPage.getAnswer('3-2')).toBe('BLANC');
    });

    it('continue button should be disabled', () => {
      expect(Main.nextStepButton.enabled).toBeFalsy('continue button should be disabled');
    });

    it('modify button should navigate to ballot page', () => {
      SummaryPage.modifyButton.click();
      expect(Main.stepTitleText).toBe('ÉTAPE 3 : BULLETIN DE VOTE');
    });

    it('user\'s choices should still be the same', () => {
      expect(BallotPage.getCheckbox(1).checked).toBeTruthy();
      expect(BallotPage.getCheckbox(2).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(4).checked).toBeTruthy();
      expect(BallotPage.getCheckbox(5).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(7).checked).toBeTruthy();
      expect(BallotPage.getCheckbox(8).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(10).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(11).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(13).checked).toBeTruthy();
      expect(BallotPage.getCheckbox(14).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(16).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(17).checked).toBeTruthy();
      expect(BallotPage.getCheckbox(19).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(20).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(22).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(23).checked).toBeFalsy();
      Main.nextStepButton.click();
      expect(SummaryPage.isPresent()).toBeTruthy();
    });

    it('accepting conditions should not enable the continue button until birth date is entered', () => {
      SummaryPage.acceptCheckbox.click();
      expect(Main.nextStepButton.enabled).toBeFalsy('continue button should be disabled');
    });

    it("an error should be displayed if the birthday format is incorrect", () => {
      SummaryPage.birthDateInput.value = '124578';
      SummaryPage.acceptCheckbox.click();
      expect(SummaryPage.birthDateInput.error).toEqual('Format incorrect');
    });

    it("the error should disappear if the birthday format is correct", () => {
      SummaryPage.birthDateInput.value = '25/08/1990';
      expect(SummaryPage.birthDateInput.errorIsPresent).toBeFalsy('no error should be displayed')
    });

    it('accepting conditions should enable the continue button', () => {
      SummaryPage.acceptCheckbox.click();
      expect(Main.quitButton.enabled).toBeFalsy('quit button should be disabled');
      expect(SummaryPage.modifyButton.enabled).toBeFalsy('modify button should be disabled');
      expect(Main.nextStepButton.enabled).toBeTruthy('continue button should be enabled');
    });

    it('should prevent user to enter a wrong birth date', () => {
      Main.nextStepButton.click();
      ChvoteDialog.waitForMessage();
      expect(ChvoteDialog.message).toEqual(
        "Votre authentification a échoué. Il vous reste 4 essai(s) pour voter par internet. " +
        "Ensuite, vous ne pourrez plus voter par internet. " +
        "Vous pourrez cependant voter par correspondance ou à l'urne selon votre lieu de résidence.");
      ChvoteDialog.clickPrimary();
    });

    it('continue button should navigate to control page after birth date got fixed', () => {
      SummaryPage.birthDateInput.value = '25/08/1980';
      Main.nextStepButton.click();
      expect(Main.stepTitle.isDisplayed());
    });
  });

  describe('Control', () => {

    it('page title should be "ÉTAPE 5 : VÉRIFICATION"', () => {
      expect(Main.stepTitleText).toBe('ÉTAPE 5 : VÉRIFICATION');
    });

    Main.checkAccessibility();

    it('user choices should be displayed', () => {
      expect(ControlPage.getAnswer('1-1')).toBe('OUI');
      expect(ControlPage.getAnswer('1-2')).toBe('OUI');
      expect(ControlPage.getAnswer('1-3')).toBe('IN');
      expect(ControlPage.getAnswer('1-4')).toBe('BLANC');
      expect(ControlPage.getAnswer('2-1')).toBe('OUI');
      expect(ControlPage.getAnswer('2-2')).toBe('NON');
      expect(ControlPage.getAnswer('3-1')).toBe('BLANC');
      expect(ControlPage.getAnswer('3-2')).toBe('BLANC');
    });

    it('correct verification codes should be displayed', () => {
      expect(ControlPage.getVerificationCode('1-1')).toBe(verificationCodes[0]);
      expect(ControlPage.getVerificationCode('1-2')).toBe(verificationCodes[3]);
      expect(ControlPage.getVerificationCode('1-3')).toBe(verificationCodes[6]);
      expect(ControlPage.getVerificationCode('1-4')).toBe(verificationCodes[11]);
      expect(ControlPage.getVerificationCode('2-1')).toBe(verificationCodes[12]);
      expect(ControlPage.getVerificationCode('2-2')).toBe(verificationCodes[16]);
      expect(ControlPage.getVerificationCode('3-1')).toBe(verificationCodes[20]);
      expect(ControlPage.getVerificationCode('3-2')).toBe(verificationCodes[23]);
    });


    it('should prevent user to enter a wrong confirmation code', () => {
      ControlPage.confirmationCodeInput.value = "Wrong_confirmation_code";
      Main.nextStepButton.click();
      ChvoteDialog.waitForMessage();
      expect(ChvoteDialog.message).toEqual(
        "Votre confirmation a échoué. Il vous reste 4 essai(s) pour voter par internet. " +
        "Ensuite, vous ne pourrez plus voter par internet. Vous pourrez cependant voter " +
        "par correspondance ou à l'urne selon votre lieu de résidence."
      );
      ChvoteDialog.clickPrimary();
    });


    it('continue button should navigate to finalization page when confirmation code is correct', () => {
      ControlPage.confirmationCodeInput.value = confirmationCode;
      Main.nextStepButton.click();
      expect(Main.stepTitle.isDisplayed());
    });
  });

  describe('Finalization', () => {

    it('page title should be "ÉTAPE 6 : FINALISATION DU VOTE"', () => {
      expect(Main.stepTitleText).toBe('ÉTAPE 6 : FINALISATION DU VOTE');
    });

    Main.checkAccessibility();

    it('finalization code should be displayed', () => {
      expect(FinalizationPage.code).toBe(finalizationCode);
    });
  });

});


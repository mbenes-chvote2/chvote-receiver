/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { by, ElementFinder, protractor } from "protractor";
import { promise } from 'selenium-webdriver';

/**
 * Object used to manipulate an input field.
 */
export class InputField {

  protected field: ElementFinder;

  /**
   * Create a new input field reference
   *
   * @param {ElementFinder} container the root element containing the input field
   * @param {string} identifier the identifier
   * @param {boolean} byFormControlName whether the input field is identified by its from control name
   * @param {boolean} byName whether the input field is identified by its name attribute or directly its ID
   */
  constructor(container: ElementFinder,
              identifier: string,
              byFormControlName: boolean = true,
              byName: boolean = false) {
    this.field = container.element(byFormControlName ?
      by.css(`input[formcontrolname="${identifier}"]`) :
      byName ? by.css(`input[name="${identifier}"]`) :
        by.id(identifier)
    );
  }

  /**
   * @returns the input field's placeholder
   */
  get placeholder(): promise.Promise<string> {
    return this.field.getAttribute('placeholder');
  }

  /**
   * @returns true if the error field is present
   */
  get errorIsPresent(): promise.Promise<boolean>{
    return this.field.element(by.xpath('ancestor::mat-form-field//mat-error')).isPresent();
  }

  /**
   * @returns the input field's error message
   */
  get error(): promise.Promise<string> {
    return this.field.element(by.xpath('ancestor::mat-form-field//mat-error')).getText();
  }

  /**
   * @returns true if the field is required, false otherwise
   */
  get required(): promise.Promise<string> {
    return this.field.getAttribute('required');
  }

  /**
   * @returns true if the field is present in the DOM, false otherwise
   */
  get present(): promise.Promise<boolean> {
    return this.field.isPresent();
  }

  /**
   * @returns true if the field is enabled, false otherwise
   */
  get enabled(): promise.Promise<boolean> {
    return this.field.isEnabled();
  }

  /**
   * Sets the field's value
   *
   * @param value the value to set
   */
  set value(value: string) {
    this.field.clear().then(() => {
      // TRICK to allow update to get taken into account
      this.field.sendKeys(protractor.Key.DELETE);
      this.field.sendKeys(value)
    });
  }

  /**
   * @returns {promise.Promise<string>} the value displayed in the input field
   */
  get inputValue(): promise.Promise<string> {
    return this.field.getAttribute('value')
  }
}

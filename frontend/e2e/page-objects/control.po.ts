/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { by, element } from 'protractor';
import { promise } from 'selenium-webdriver';
import { InputField } from '../shared/input-field';

/**
 * Page object for the control page.
 */
export class ControlPage {

  /**
   * Get the displayed answer for a given subject
   *
   * @param globalIndex subject's global index
   * @return the corresponding answer
   */
  static getAnswer(globalIndex: string): promise.Promise<string> {
    return element(by.id(`summary-answer-${globalIndex}`)).getText();
  }

  /**
   * Get the displayed verification code for a given subject
   *
   * @param globalIndex subject's global index
   * @return the corresponding verification code
   */
  static getVerificationCode(globalIndex: string): promise.Promise<string> {
    return element(by.id(`verification-code-${globalIndex}`)).getText();
  }

  /**
   * @returns the input field for the confirmation code
   */
  static get confirmationCodeInput(): InputField {
    return new InputField(element(by.tagName("form")), "confirmationCode");
  }
}


/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { Canton, createUseCase, TestCaseType } from '../shared/mock-server';
import { browser, ExpectedConditions } from 'protractor';
import { Main } from '../page-objects/main.po';
import { IdentificationPage } from '../page-objects/identification.po';
import { BallotPage } from '../page-objects/ballot.po';
import { SummaryPage } from '../page-objects/summary.po';
import { ControlPage } from '../page-objects/control.po';
import { FinalizationPage } from '../page-objects/finalization.po';
import { BallotSelectionPage } from '../page-objects/ballot-selection.po';
import { ChvoteDialog } from '../page-objects/chvote-dialog.po';

describe('Multi Votation Ballot', () => {

  let protocolInstanceId;
  let voterId;
  let voterIdAndCardNumber;
  let confirmationCode;
  let verificationCodes;
  let finalizationCode;

  describe('Identification', () => {

    beforeAll((done) => {
      browser.get('/test/vote/');
      browser.controlFlow().execute(
        () => createUseCase(false, true, TestCaseType.TEST, Canton.GE, false)
          .then(useCaseMetaData => {
              protocolInstanceId = useCaseMetaData.protocolInstanceId;
              voterId = useCaseMetaData.votingCards[1].voterId;
              voterIdAndCardNumber = `${voterId}:${useCaseMetaData.votingCards[1].cardNumber}`;
              confirmationCode = useCaseMetaData.votingCards[1].confirmationCode;
              verificationCodes = useCaseMetaData.votingCards[1].verificationCodes;
              finalizationCode = useCaseMetaData.votingCards[1].finalizationCode;
            }
          )
          .then(done)
      );
    });

    it('should accept to identify the voter', () => {
      Main.navigateTo(`/test/vote/ge/${protocolInstanceId}/fr/identification`);
      IdentificationPage.cardNumberInput.value = voterIdAndCardNumber;
      Main.nextStepButton.click();
      expect(Main.stepTitle.isDisplayed());
    });

  });

  describe('Legal Information', () => {

    it('continue button should navigate to the ballot selection page', () => {
      Main.nextStepButton.click();
      expect(Main.stepTitle.isDisplayed());
    });
  });

  describe('Ballot Selection', () => {

    it('page title should be "ÉTAPE 3 : BULLETIN DE VOTE"', () => {
      expect(Main.stepTitleText).toBe('ÉTAPE 3 : BULLETIN DE VOTE');
    });

    Main.checkAccessibility('heading-order');

    it('should display two votation ballots', () => {
      expect(BallotSelectionPage.ballotCount).toBe(2);
    });

    it('should state that no ballot is filled', () => {
      expect(BallotSelectionPage.description).toBe('0 scrutin(s).');
    });

    it('first ballot title should be "VOTATION FÉDÉRALE"', () => {
      expect(BallotSelectionPage.getBallotTitle(0)).toBe('VOTATION FÉDÉRALE');
    });

    it('second ballot title should be "VOTATION CANTONALE"', () => {
      expect(BallotSelectionPage.getBallotTitle(1)).toBe('VOTATION CANTONALE');
    });

    it('first ballot number of subjects should be 2', () => {
      expect(BallotSelectionPage.getSubjectCount(0)).toBe('2 objets');
    });

    it('second ballot number of subjects should be 2"', () => {
      expect(BallotSelectionPage.getSubjectCount(1)).toBe('2 objets');
    });

    it('participate button should display a warning if no ballot is filled', () => {
      Main.nextStepButton.click();
      browser.wait(ExpectedConditions.visibilityOf(ChvoteDialog.dialog));
      expect(ChvoteDialog.message)
        .toBe('Pour pouvoir passer à l\'étape suivante, vous devez participer à au moins 1 scrutin.');
      ChvoteDialog.clickPrimary();
    });

    it('continue button should navigate to ballot page', () => {
      BallotSelectionPage.getParticipateButton(0).click();
      expect(Main.stepTitle.isDisplayed());
    });

    describe('Ballot', () => {

      it('all answers should be blank', () => {
        expect(BallotPage.getCheckbox(1).checked).toBeFalsy();
        expect(BallotPage.getCheckbox(2).checked).toBeFalsy();
        expect(BallotPage.getCheckbox(4).checked).toBeFalsy();
        expect(BallotPage.getCheckbox(5).checked).toBeFalsy();
        expect(BallotPage.getCheckbox(7).checked).toBeFalsy();
        expect(BallotPage.getCheckbox(8).checked).toBeFalsy();
        expect(BallotPage.getCheckbox(10).checked).toBeFalsy();
        expect(BallotPage.getCheckbox(11).checked).toBeFalsy();
      });

      it('continue button label should be "VALIDER LE BULLETIN"', () => {
        expect(Main.nextStepButton.text).toBe('VALIDER LE BULLETIN');
      });

      it('back button should navigate back to ballot selection page if no selection was made', () => {
        BallotPage.backButton.click();
        expect(BallotSelectionPage.description).toBe('0 scrutin(s).');
      });

      it('back button should display a warning message if selections were made', () => {
        BallotSelectionPage.getParticipateButton(0).click();
        BallotPage.getCheckbox(1).click();
        BallotPage.getCheckbox(4).click();
        BallotPage.getCheckbox(8).click();
        BallotPage.backButton.click();

        browser.wait(ExpectedConditions.visibilityOf(ChvoteDialog.dialog));
        expect(ChvoteDialog.message)
          .toBe('Cette action annulera toutes les modifications effectuées sur la page, souhaitez-vous continuer ?');
        ChvoteDialog.clickSecondary();
      });

      it('continue button should navigate back to ballot selection page', () => {
        Main.nextStepButton.click();
        expect(BallotSelectionPage.description).toBe('1 scrutin(s).');
      });

      it('ballot should be marked as "filled"', () => {
        expect(BallotSelectionPage.isFilledMarkerPresent(0)).toBeTruthy('filled marker should be present');
        expect(BallotSelectionPage.getCancelParticipateButton(0).displayed)
          .toBeTruthy('cancel participation button should be displayed');
        expect(BallotSelectionPage.getModifyButton(0).displayed).toBeTruthy('modify button should be displayed');
      });
    });

    it('modify button should navigate to ballot page', () => {
      BallotSelectionPage.getModifyButton(0).click();
    });

    it('user\'s choices should still be the same', () => {
      expect(BallotPage.getCheckbox(1).checked).toBeTruthy();
      expect(BallotPage.getCheckbox(2).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(4).checked).toBeTruthy();
      expect(BallotPage.getCheckbox(5).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(7).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(8).checked).toBeTruthy();
      expect(BallotPage.getCheckbox(10).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(11).checked).toBeFalsy();
      BallotPage.backButton.click();
    });

    it('cancel participation button should display a warning message if only one ballot is filled', () => {
      BallotSelectionPage.getCancelParticipateButton(0).click();
      browser.wait(ExpectedConditions.visibilityOf(ChvoteDialog.dialog));
      expect(ChvoteDialog.message).toBe(
        'Vous ne pouvez pas annuler votre participation à ce scrutin car il n\'y aurait plus aucune participation dans votre bulletin de vote.');
      ChvoteDialog.clickPrimary();
    });

    it('fill the second ballot', () => {
      BallotSelectionPage.getParticipateButton(1).click();
      BallotPage.getCheckbox(14).click();
      BallotPage.getCheckbox(17).click();
      Main.nextStepButton.click();
      expect(BallotSelectionPage.description).toBe('2 scrutin(s).');
      expect(BallotSelectionPage.isFilledMarkerPresent(1)).toBeTruthy('filled marker should be present');
    });

    it('cancel participation button should display a warning message', () => {
      BallotSelectionPage.getCancelParticipateButton(1).click();
      browser.wait(ExpectedConditions.visibilityOf(ChvoteDialog.dialog));
      expect(ChvoteDialog.message)
        .toBe('Cette action annulera votre participation au scrutin votation cantonale, souhaitez-vous continuer ?');
    });

    it('answering "No" should not cancel the participation', () => {
      ChvoteDialog.clickSecondary();
      expect(BallotSelectionPage.isFilledMarkerPresent(1)).toBeTruthy('filled marker should still be present');
    });

    it('answering "Yes" should cancel the participation', () => {
      BallotSelectionPage.getCancelParticipateButton(1).click();
      ChvoteDialog.clickPrimary();
      expect(BallotSelectionPage.isFilledMarkerPresent(1)).toBeFalsy('filled marker should not be present');
      expect(BallotSelectionPage.getParticipateButton(1).displayed)
        .toBeTruthy('participate button should be displayed');
    });

    it('continue button should display a warning message if not all ballots were filled', () => {
      Main.nextStepButton.click();
      browser.wait(ExpectedConditions.visibilityOf(ChvoteDialog.dialog));
      expect(ChvoteDialog.message).toBe('Vous n\'avez pas participé à tous les scrutins !');
      ChvoteDialog.clickSecondary();
    });

    it('continue button should navigate to summary page', () => {
      BallotSelectionPage.getParticipateButton(1).click();
      BallotPage.getCheckbox(14).click();
      BallotPage.getCheckbox(17).click();
      Main.nextStepButton.click();
      Main.nextStepButton.click();
      expect(Main.stepTitle.isDisplayed());
    });
  });

  describe('Summary', () => {

    it('page title should be "ÉTAPE 4 : RÉCAPITULATIF"', () => {
      expect(Main.stepTitleText).toBe('ÉTAPE 4 : RÉCAPITULATIF');
    });

    Main.checkAccessibility();

    it('user choices should be displayed', () => {
      expect(SummaryPage.getAnswer('1-1')).toBe('OUI');
      expect(SummaryPage.getAnswer('1-2')).toBe('OUI');
      expect(SummaryPage.getAnswer('1-3')).toBe('CP');
      expect(SummaryPage.getAnswer('1-4')).toBe('BLANC');
      expect(SummaryPage.getAnswer('2-1')).toBe('NON');
      expect(SummaryPage.getAnswer('2-2')).toBe('NON');
    });

    it('continue button should be disabled', () => {
      expect(Main.nextStepButton.enabled).toBeFalsy('continue button should be disabled');
    });

    it('modify button should navigate to ballot page', () => {
      SummaryPage.getModifyButton(0).click();
      expect(Main.stepTitleText).toBe('ÉTAPE 3 : BULLETIN DE VOTE');
    });

    it('user\'s choices should still be the same', () => {
      expect(BallotPage.getCheckbox(1).checked).toBeTruthy();
      expect(BallotPage.getCheckbox(2).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(4).checked).toBeTruthy();
      expect(BallotPage.getCheckbox(5).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(7).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(8).checked).toBeTruthy();
      expect(BallotPage.getCheckbox(10).checked).toBeFalsy();
      expect(BallotPage.getCheckbox(11).checked).toBeFalsy();
    });

    it('ballot validation should navigate back to summary page', () => {
      Main.nextStepButton.click();
      expect(Main.stepTitleText).toBe('ÉTAPE 4 : RÉCAPITULATIF');
    });

    it('back button should navigate back to ballot selection page', () => {
      BallotPage.backButton.click();
      expect(Main.stepTitleText).toBe('ÉTAPE 3 : BULLETIN DE VOTE');
      Main.nextStepButton.click();
    });

    it('cancel participation button should display a warning message', () => {
      SummaryPage.getCancelParticipateButton(0).click();
      browser.wait(ExpectedConditions.visibilityOf(ChvoteDialog.dialog));
      expect(ChvoteDialog.message)
        .toBe('Cette action annulera votre participation au scrutin votation fédérale, souhaitez-vous continuer ?');
    });

    it('answering "No" should not cancel the participation', () => {
      ChvoteDialog.clickSecondary();
      expect(SummaryPage.getCancelParticipateButton(0).displayed)
        .toBeTruthy('cancel participation button should still be displayed');
    });

    it('answering "Yes" should cancel the participation', () => {
      SummaryPage.getCancelParticipateButton(0).click();
      ChvoteDialog.clickPrimary();
      expect(SummaryPage.getCancelParticipateButton(0).displayed)
        .toBeFalsy('cancel participation button should be hidden');
      expect(SummaryPage.getEmptyBallotDescription(0).isDisplayed)
        .toBeTruthy('empty ballot description should be displayed');
      expect(SummaryPage.getEmptyBallotDescription(0).getText())
        .toBe('Je ne souhaite pas soumettre de bulletin pour ce scrutin');
    });

    it('re-fill the cancelled ballot', () => {
      BallotPage.backButton.click();
      BallotSelectionPage.getParticipateButton(0).click();
      BallotPage.getCheckbox(1).click();
      BallotPage.getCheckbox(4).click();
      BallotPage.getCheckbox(8).click();
      Main.nextStepButton.click();
      Main.nextStepButton.click();
    });

    it('continue button should navigate to control page', () => {
      SummaryPage.birthDateInput.value = '08/1980';
      SummaryPage.acceptCheckbox.click();
      Main.nextStepButton.click();
      expect(Main.stepTitle.isDisplayed());
    });
  });

  describe('Control', () => {

    it('page title should be "ÉTAPE 5 : VÉRIFICATION"', () => {
      expect(Main.stepTitleText).toBe('ÉTAPE 5 : VÉRIFICATION');
    });

    it('user choices should be displayed', () => {
      expect(ControlPage.getAnswer('1-1')).toBe('OUI');
      expect(ControlPage.getAnswer('1-2')).toBe('OUI');
      expect(ControlPage.getAnswer('1-3')).toBe('CP');
      expect(ControlPage.getAnswer('1-4')).toBe('BLANC');
      expect(ControlPage.getAnswer('2-1')).toBe('NON');
      expect(ControlPage.getAnswer('2-2')).toBe('NON');
    });

    it('correct verification codes should be displayed', () => {
      expect(ControlPage.getVerificationCode('1-1')).toBe(verificationCodes[0]);
      expect(ControlPage.getVerificationCode('1-2')).toBe(verificationCodes[3]);
      expect(ControlPage.getVerificationCode('1-3')).toBe(verificationCodes[7]);
      expect(ControlPage.getVerificationCode('1-4')).toBe(verificationCodes[11]);
      expect(ControlPage.getVerificationCode('2-1')).toBe(verificationCodes[13]);
      expect(ControlPage.getVerificationCode('2-2')).toBe(verificationCodes[16]);
    });

    it('continue button should navigate to finalization page', () => {
      ControlPage.confirmationCodeInput.value = confirmationCode;
      Main.nextStepButton.click();
      expect(Main.stepTitle.isDisplayed());
    });
  });

  describe('Finalization', () => {

    it('page title should be "ÉTAPE 6 : FINALISATION DU VOTE"', () => {
      expect(Main.stepTitleText).toBe('ÉTAPE 6 : FINALISATION DU VOTE');
    });

    it('finalization code should be displayed', () => {
      expect(FinalizationPage.code).toBe(finalizationCode);
    });
  });

});


/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
import { SideZoneComponent } from "./side-zone/side-zone.component";
import { HeaderZoneComponent } from "./header-zone/header-zone.component";
import { FooterZoneComponent } from "./footer-zone/footer-zone.component";
import { OperationComponent } from "./operation/operation.component";
import { VoteWorkflowStepComponent } from './operation/vote-workflow-display/vote-workflow-step.component';
import { VoteWorkflowViewComponent } from './operation/vote-workflow-display/vote-workflow-view.component';
import { SideZoneSectionComponent } from './side-zone/side-zone-section/side-zone-section.component';
import { CoreModule } from './core/core.module';
import { CurrentVotingService } from './operation/current-voting-service';
import { OperationService } from './operation/operation.service';
import { TranslationResolver } from './operation/translation.resolver';
import { IdentificationComponent } from './operation/1-identification/identification.component';
import { LegalInformationComponent } from './operation/2-legal-information/legal-information.component';
import { BallotsListComponent } from './operation/3-ballots-list/ballots-list.component';
import { IdentificationService } from './operation/1-identification/identification.service';
import { SummaryComponent } from "./operation/4-summary/summary.component";
import { ControlComponent } from './operation/5-control/control.component';
import { FinalizationComponent } from './operation/6-finalization/finalization.component';
import { SingleBallotCardComponent } from './operation/3-ballots-list/single-ballot-card/single-ballot-card.component';
import { GroupedVotationCardComponent } from './operation/3-ballots-list/grouped-votation-card/grouped-votation-card.component';
import { VotationCardComponent } from './operation/3-ballots-list/grouped-votation-card/votation-card/votation-card.component';
import { VotationBallotComponent } from './operation/3-ballots-list/votation-ballot/votation-ballot.component';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr-CH';
import localeDe from '@angular/common/locales/de-CH';
import localeIt from '@angular/common/locales/it-CH';
import localeRm from '@angular/common/locales/rm';
import { ErrorMessageComponent } from './core/error-message/error-message.component';
import { OperationResolver } from './operation/operation.resolver';
import { FooterDocumentsComponent } from './footer-zone/footer-documents.component';

registerLocaleData(localeFr, 'fr');
registerLocaleData(localeDe, 'de');
registerLocaleData(localeIt, 'it');
registerLocaleData(localeRm, 'rm');

@NgModule({
  declarations: [
    AppComponent,
    OperationComponent,
    IdentificationComponent,
    LegalInformationComponent,
    SideZoneComponent,
    VoteWorkflowStepComponent,
    HeaderZoneComponent,
    FooterZoneComponent,
    FooterDocumentsComponent,
    BallotsListComponent,
    VoteWorkflowViewComponent,
    SideZoneSectionComponent,
    SingleBallotCardComponent,
    GroupedVotationCardComponent,
    VotationCardComponent,
    VotationBallotComponent,
    SummaryComponent,
    ControlComponent,
    FinalizationComponent,
    ErrorMessageComponent
  ],
  imports: [
    AppRoutingModule,
    CoreModule,
  ],
  exports: [
    CoreModule
  ],
  providers: [
    IdentificationService,
    TranslationResolver,
    OperationResolver,
    CurrentVotingService,
    OperationService,
    VoteWorkflowViewComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ValidationErrorModel } from './validation.error.model';
import { TechnicalErrorModel } from './technical.error.model';
import { HttpParameters } from './http.parameters.service';
import { MatDialog } from '@angular/material';

import { DateInterceptor } from './DateInterceptor';
import { TechnicalErrorDialogComponent } from '../../technical-error-dialog/technical-error-dialog.component';
import { BusinessErrorDialogComponent } from '../business-error-dialog/business-error-dialog.component';

/**
 * Handle http call response errors. Distinguish Business errors, technical server errors and other errors.
 *
 * A business errors (http status 400) is left to process by the initiator (typically a component controller).
 * If not handled (ie 'errorHandled' property of error object is still false after initiator's error callback
 * execution), then show a modal dialog (BusinessErrorDialogComponent)
 *
 * Any technical error (http status 500) triggers the display of a modal dialog (TechnicalErrorDialogComponent) and
 * then is handed to the initiator' error callback
 *
 * Other errors are handed to the initiator' error callback and then an Error is thrown
 * @param httpCall
 * @returns {any}
 */
@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  technicalErrorDialogIsOpen = false;


  constructor(private httpParams: HttpParameters, private dialog: MatDialog) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return Observable.create(subscriber => {
      next.handle(req).subscribe(
        res => subscriber.next(res),
        err => {
          let errorProvider: HttpRequestErrorProvider;
          let error: ValidationErrorModel | TechnicalErrorModel;
          if (err.status === 400) {
            error = JSON.parse(err.error, DateInterceptor.parseDateFields) as ValidationErrorModel;
            this.httpParams.translateMessages(error);
            errorProvider = new HttpRequestErrorProvider(ErrorType.Validation, error);
          } else {

            if (err.status === 500) {
              error = JSON.parse(err.error, DateInterceptor.parseDateFields);
            } else if (err.status === 403) {
              error = new TechnicalErrorModel(err.status, "global.errors.forbidden");
            } else {
              error = new TechnicalErrorModel(err.status, "global.errors.unknown")
            }

            errorProvider = new HttpRequestErrorProvider(ErrorType.Technical, error);
            if (err.status === 401) {
              errorProvider._handled = true;
            }
          }
          try {
            subscriber.error(errorProvider);
          }
          finally {
            if (!errorProvider._handled) {
              if (errorProvider._type == ErrorType.Technical) {
                if (!this.technicalErrorDialogIsOpen) {
                  this.technicalErrorDialogIsOpen = true;
                  let dialog = this.dialog.open(TechnicalErrorDialogComponent, {data: error});
                  dialog.afterClosed().subscribe(() => {
                    this.technicalErrorDialogIsOpen = false
                  })
                }

              } else {
                this.dialog.open(BusinessErrorDialogComponent, {data: error});
              }
            }
          }
        }
      )
    });
  };
}


enum ErrorType {
  Technical, Validation
}

export class HttpRequestErrorProvider {
  _handled = false;

  constructor(public _type: ErrorType, private error: TechnicalErrorModel | ValidationErrorModel) {

  }

  onTechnicalError(errorKey: string, handler: (error: TechnicalErrorModel) => void) {
    if (this._type === ErrorType.Technical && (<TechnicalErrorModel>this.error).message === errorKey) {
      handler(<TechnicalErrorModel>this.error);
      this._handled = true;
    }
    return this;
  }

  onValidationError(exceptionClassNameFilter: RegExp | string, handler: (error: ValidationErrorModel) => void) {
    if (this._handled) {
      return this;
    }
    let shouldHandle = false;

    if (this._type !== ErrorType.Validation) {
      return this;
    }


    let exceptionClassName = (<ValidationErrorModel>this.error).exceptionClassName;


    if (exceptionClassNameFilter instanceof RegExp) {
      if (exceptionClassNameFilter.test(exceptionClassName)) {
        shouldHandle = true;
      }
    } else {
      if (exceptionClassNameFilter === exceptionClassName) {
        shouldHandle = true;
      }
    }

    if (shouldHandle) {
      handler(<ValidationErrorModel>this.error);
      this._handled = true;
    }

    return this;
  }

}

/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { Component, Input } from "@angular/core";
import { Votation } from '../../model/votation';
import { TranslateService } from '@ngx-translate/core';
import { ElectionBallot } from '../../model/election';

@Component({
  selector: 'chvote-ballot-header',
  templateUrl: './chvote-ballot-header.component.html',
  styleUrls: ['./chvote-ballot-header.component.scss']
})
export class ChvoteBallotHeaderComponent {

  @Input()
  display: 'ballot' | 'summary' | 'control';

  @Input()
  ballot: Votation | ElectionBallot;

  @Input()
  empty: boolean;

  constructor(private translateService: TranslateService) {
  }

  getDocumentations() {
    return this.ballot.documentations.filter(
      doc => doc.lang.toLowerCase() === this.translateService.currentLang);
  }
}

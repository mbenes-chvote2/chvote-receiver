/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { Component, Input } from "@angular/core";
import { Votation } from '../../model/votation';
import { CurrentVotingService } from '../../operation/current-voting-service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { ChvoteDialogComponent } from '../chvote-dialog/chvote-dialog.component';

@Component({
  selector: 'chvote-votation-ballot',
  templateUrl: './chvote-votation-ballot.component.html',
  styleUrls: ['./chvote-votation-ballot.component.scss']
})
export class ChvoteVotationBallotComponent {

  @Input()
  display: 'ballot' | 'summary' | 'control';

  @Input()
  votations: Votation[];

  @Input()
  verificationCodes: string[] = [];

  @Input()
  buttonDisabled: boolean = false;

  @Input()
  ballotNumber: number = 0;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private currentVotingService: CurrentVotingService,
              private dialog: MatDialog) {
  }

  get isEmpty(): boolean {
    for (let votation of this.votations) {
      if (this.currentVotingService.hasVotedFor(votation)) {
        return false;
      }
    }
    return true;
  }

  clean(): void {
    // display confirmation message
    this.dialog.open(ChvoteDialogComponent, {
      width: '500px',
      disableClose: true,
      data: {
        messageKey: 'ballot-list.dialog.confirm-cancel',
        primaryButtonKey: 'global.actions.yes',
        secondaryButtonKey: 'global.actions.no'
      }
    }).afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        CurrentVotingService.initVotationBallot(this.votations);
      }
    });
  }
}

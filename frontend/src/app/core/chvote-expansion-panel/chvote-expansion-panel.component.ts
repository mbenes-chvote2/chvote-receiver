/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { Component, Input } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'chvote-expansion-panel',
  templateUrl: './chvote-expansion-panel.component.html',
  styleUrls: ['./chvote-expansion-panel.component.scss'],
  animations: [
    trigger('icon-animation', [
      state('hidden', style({
        transform: 'rotate(0)'
      })),
      state('displayed', style({
        transform: 'rotate(-180deg)'
      })),
      transition('hidden <=> displayed', animate(250))
    ]),
    trigger('content-animation', [
      state('hidden', style({
        height: '0px'
      })),
      state('displayed', style({
        height: '*'
      })),
      transition('hidden <=> displayed', animate(250))
    ])
  ]
})
export class ChvoteExpansionPanelComponent {

  @Input()
  accessibilityKey: string;

  @Input()
  titleKey: string;

  @Input()
  contentKey: string;

  @Input()
  strongerContrast: boolean = false;

  state = 'hidden';

  toggle(): void {
    this.state = this.state === 'displayed' ? 'hidden' : 'displayed';
  }
}

/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { NgModule } from "@angular/core";
import { CommonModule, DatePipe } from "@angular/common";
import { BusinessErrorDialogComponent } from "./business-error-dialog/business-error-dialog.component";
import {
  MatButtonModule, MatCheckboxModule, MatDialogModule, MatExpansionModule, MatFormFieldModule, MatIconModule,
  MatInputModule
} from "@angular/material";
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { TechnicalErrorDialogComponent } from '../technical-error-dialog/technical-error-dialog.component';
import { BaseUrlService } from './base-url/base-url.service';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { ExternalConfigurationTranslateLoader } from './ExternalConfigurationTranslateLoader';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpParameters } from './http/http.parameters.service';
import { DateInterceptor } from './http/DateInterceptor';
import { ErrorInterceptor } from './http/ErrorInterceptor';
import { NoCacheInterceptor } from './http/NoCacheInterceptor';
import { DefaultErrorsComponent } from './default-errors/default-errors.directive';
import { HeaderInterceptor } from './service/http/header-interceptor.service';
import { AssetService } from './service/asset.service';
import { ProtocolService } from './service/protocol.service';
import { Blake2Module } from '@protocoder/blake2';
import { ChvoteDialogComponent } from './chvote-dialog/chvote-dialog.component';
import { ChvoteCheckboxComponent } from './chvote-checkbox/chvote-checkbox.component';
import { ChvoteExpansionPanelComponent } from './chvote-expansion-panel/chvote-expansion-panel.component';
import { ChvoteDocumentsComponent } from './chvote-documents/chvote-documents.component';
import { ChvoteCoatOfArmsComponent } from './chvote-coat-of-arms/chvote-coat-of-arms.component';
import { ChvoteSubjectCardComponent } from './chvote-votation-ballot/chvote-subject-card/chvote-subject-card.component';
import { ChvoteVariantSubjectCardComponent } from './chvote-votation-ballot/chvote-variant-subject-card/chvote-variant-subject-card.component';
import { ChvoteVotationBallotComponent } from './chvote-votation-ballot/chvote-votation-ballot.component';
import { ChvoteBallotHeaderComponent } from './chvote-ballot-header/chvote-ballot-header.component';
import { ChvoteHelperComponent } from './chvote-helper/chvote-helper.component';
import { ActionsOnBallotsComponent } from './actions-on-ballots/actions-on-ballots.component';
import { FinalizationService } from './service/finalization.service';
import { LocalizedLabelComponent } from './localized-label/localized-label.component';
import { LocalizationService } from './service/localization.service';
import { DocumentationService } from './service/documentation.service';


@NgModule({
  imports: [
    CommonModule,
    MatIconModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useExisting: ExternalConfigurationTranslateLoader
      }
    }),
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatButtonModule,
    HttpClientModule,
    Blake2Module
  ],
  declarations: [
    TechnicalErrorDialogComponent, BusinessErrorDialogComponent, LocalizedLabelComponent, DefaultErrorsComponent,
    ActionsOnBallotsComponent, ChvoteDialogComponent, ChvoteCheckboxComponent, ChvoteExpansionPanelComponent,
    ChvoteDocumentsComponent, ChvoteCoatOfArmsComponent, ChvoteBallotHeaderComponent, ChvoteVotationBallotComponent,
    ChvoteVariantSubjectCardComponent, ChvoteSubjectCardComponent, ChvoteHelperComponent
  ],
  entryComponents: [TechnicalErrorDialogComponent, BusinessErrorDialogComponent, ChvoteDialogComponent],
  providers: [
    ExternalConfigurationTranslateLoader,
    DatePipe,
    HttpParameters,
    BaseUrlService,
    AssetService,
    LocalizationService,
    ProtocolService,
    FinalizationService,
    DocumentationService,
    {provide: HTTP_INTERCEPTORS, useClass: DateInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: NoCacheInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: HeaderInterceptor, multi: true}
  ],

  exports: [
    CommonModule,
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatExpansionModule,
    MatButtonModule,
    HttpClientModule,
    MatDialogModule,
    MatCheckboxModule,
    MatIconModule,
    TranslateModule,
    LocalizedLabelComponent,
    DefaultErrorsComponent,
    ActionsOnBallotsComponent,
    ChvoteDialogComponent,
    ChvoteCheckboxComponent,
    ChvoteExpansionPanelComponent,
    ChvoteDocumentsComponent,
    ChvoteCoatOfArmsComponent,
    ChvoteBallotHeaderComponent,
    ChvoteVotationBallotComponent,
    ChvoteVariantSubjectCardComponent,
    ChvoteSubjectCardComponent,
    ChvoteHelperComponent
  ]
})
export class CoreModule {
}

/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { Ballot } from './ballot';

export interface ElectionBallot extends Ballot {
  elections: Election[];
  displayCandidateSearchForm: boolean;
  allowChangeOfElectoralList: boolean;
  displayEmptyPosition: boolean;
  displayCandidatePositionOnACompactBallotPaper: boolean;
  displayCandidatePositionOnAModifiedBallotPaper: boolean;
  displaySuffrageCount: boolean;
  displayListVerificationCode: boolean;
  candidateInformationDisplayModel: string;
  allowOpenCandidature: boolean;
  allowMultipleMandates: boolean;
  displayVoidOnEmptyBallotPaper: boolean;
  displayedColumnsOnVerificationTable: string[];
  columnsOrderOnVerificationTable: string[];
}

export interface Candidates {
  protocolIndex: number;
  virtual: boolean;
  candidateIdentifier: string;
  lastName: string;
  firstName: string;
  localizedLabel: {
    [key: string]: string
  };
  isSelected: boolean;
}

export interface ElectionList {
  indenture: string;
  localizedLabel: {
    [key: string]: string
  };
  candidateIdentifier: string[];
}

export interface Election {
  numberOfSelections: number;
  localizedLabel: {
    [key: string]: string
  };
  candidates: Candidates[];
  electionLists: ElectionList[];
}

/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { HighlightedQuestion } from '../model/documentation';
import { CurrentVotingService } from '../operation/current-voting-service';
import { HttpParameters } from '../core/http/http.parameters.service';
import { DocumentationService } from '../core/service/documentation.service';

@Component({
  selector: 'side-zone',
  templateUrl: './side-zone.component.html',
  styleUrls: ['./side-zone.component.scss']
})
export class SideZoneComponent {

  @Input()
  showHighlightedQuestion = true;

  constructor(private httpParams: HttpParameters,
              private router: Router, private route: ActivatedRoute,
              private translateService: TranslateService,
              private currentVotingService: CurrentVotingService,
              private documentationService: DocumentationService) {
  }

  get currentLang() {
    return this.translateService.currentLang;
  }

  redirect(event, lang: string) {
    event.preventDefault();
    let snapshot = this.route.snapshot;
    let pathSegments = snapshot.url.map(segment => segment.path);
    pathSegments[pathSegments.length - 1] = lang;
    snapshot.children.forEach(child => child.url.map(segment => segment.path).forEach(path => pathSegments.push(path)));
    this.router.navigate(pathSegments)
  }

  /**
   * @return true if highlighted questions are defined for the operation
   */
  get hasHighlightedQuestions(): boolean {
    return this.showHighlightedQuestion && this.highlightedQuestions !== undefined;
  }

  /**
   * @return true if highlighted questions are defined for the operation
   * and the currently selected language
   */
  get hasHighlightedQuestionsForCurrentLang(): boolean {
    return this.currentVotingService.operation.highlightedQuestions.filter(doc => {
      return doc.lang.toLowerCase() === this.translateService.currentLang;
    }).length > 0;
  }

  /**
   * Get the URL to download the given highlighted question document
   *
   * @param doc the highlighted question document object
   * @return the corresponding download URL
   */
  getHighlightedQuestionUrl(doc: HighlightedQuestion): string {
    return this.documentationService.getDocumentationUrl(doc.fileIndex);
  }

  /**
   * @return the highlighted questions linked to the operation and the selected language.
   * If no highlighted question is defined for the selected language, the ones for the operation's
   * default language shall be returned and if no highlighted question is defined for the operation's
   * default language, "undefined" is returned
   */
  get highlightedQuestions(): HighlightedQuestion[] {
    // search based on selected lang
    let docs: HighlightedQuestion[] = this.currentVotingService.operation.highlightedQuestions.filter(doc => {
      return doc.lang.toLowerCase() === this.translateService.currentLang;
    });

    // if not found, search based on default lang
    if (docs.length === 0) {
      docs = this.currentVotingService.operation.highlightedQuestions.filter(doc => {
        return doc.lang.toLowerCase() === this.currentVotingService.operation.defaultLang.toLowerCase();
      });
    }

    return docs.length > 0 ? docs : undefined;
  }

}

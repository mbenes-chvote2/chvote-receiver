/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { Component, Input } from "@angular/core";
import { Votation } from '../../../model/votation';
import { CurrentVotingService } from '../../current-voting-service';
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: '[grouped-votation-card]',
  templateUrl: './grouped-votation-card.component.html',
  styleUrls: ['./grouped-votation-card.component.scss', '../shared.scss']
})

export class GroupedVotationCardComponent {
  @Input() ballots: Votation[];

  constructor(private router: Router,
              private route: ActivatedRoute,
              private currentVotingService: CurrentVotingService) {
  }

  get ballotFilled(): boolean {
    for (let ballot of this.ballots) {
      if (!this.currentVotingService.hasVotedFor(ballot)) {
        return false;
      }
    }
    return true;
  }

  participate(): void {
    CurrentVotingService.initVotationBallot(this.ballots);
    this.currentVotingService.lastVisitedPage = 'BALLOT_SELECTION';
    this.router.navigate(['../votationBallot'], {relativeTo: this.route});
  }
}


/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { Component, OnInit } from "@angular/core";
import { Step, VoteWorkflowViewComponent } from "../vote-workflow-display/vote-workflow-view.component";
import { ProtocolService } from '../../core/service/protocol.service';
import { FinalizationLogData } from '../../model/finalization-log-data';
import { CurrentVotingService } from '../current-voting-service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'finalization, [finalization]',
  templateUrl: './finalization.component.html',
  styleUrls: ['./finalization.component.scss']
})

export class FinalizationComponent implements OnInit {
  currentStepNumber;

  constructor(private voteWorkflowViewComponent: VoteWorkflowViewComponent,
              private protocolService: ProtocolService,
              private currentVotingService: CurrentVotingService,
              private translateService: TranslateService) {
  }

  ngOnInit(): void {
    this.currentStepNumber = this.voteWorkflowViewComponent.getStepNumber(Step.Finalization);
    this.currentVotingService.lastVisitedPage = 'FINALIZATION';
  }

  get locale(): string {
    return this.translateService.currentLang;
  }

  get finalizationLog(): FinalizationLogData {
    return this.currentVotingService.finalizationLog;
  }

  quit() {
    // TODO
  }
}

/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { Component, OnInit } from "@angular/core";
import { Step, VoteWorkflowViewComponent } from "../vote-workflow-display/vote-workflow-view.component";
import { ActivatedRoute, Router } from "@angular/router";
import { CurrentVotingService } from "../current-voting-service";
import { ProtocolService } from '../../core/service/protocol.service';
import { Votation } from '../../model/votation';
import { ElectionBallot } from '../../model/election';
import { FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material';
import { ChvoteDialogComponent } from '../../core/chvote-dialog/chvote-dialog.component';


@Component({
  selector: 'summary, [summary]',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})

export class SummaryComponent implements OnInit {
  currentStepNumber;
  nextStepNumber;
  nextStepChecked: boolean = false;
  busy: boolean = false;
  authenticationForm: FormGroup;
  birthDateScope: "DAY" | "MONTH" | "YEAR";


  constructor(private router: Router,
              private route: ActivatedRoute,
              private voteWorkflowViewComponent: VoteWorkflowViewComponent,
              private currentVotingService: CurrentVotingService,
              private protocolService: ProtocolService,
              private translateService: TranslateService,
              private dialog: MatDialog,
              private formBuilder: FormBuilder) {
  }


  private createForm() {
    this.authenticationForm = this.formBuilder.group({
      birthDate: [
        null,
        [
          Validators.required,
          this.birthDateCheck()
        ]
      ]
    });
  }

  private birthDateCheck(): ValidatorFn {
    return control => {
      if (!this.extractDate(`${control.value}`)) {
        return {'birth-date.wrong-format': this.birthDateScope}
      }
      return null
    }
  }

  extractDate(rawDateValue) {
    let parts = rawDateValue.split(/[-. /\\]/);
    let expectedElementsCount = this.birthDateScope == "DAY" ? 3 : this.birthDateScope == "MONTH" ? 2 : 1;
    if (expectedElementsCount != parts.length) {
      return null;
    }

    if (!parts.every(part => !!part.match(/[0-9]+/))) {
      return null;
    }

    let day = (expectedElementsCount == 3) ? parseInt(parts.splice(0, 1)[0]) : 1;
    let month = (expectedElementsCount >= 2) ? parseInt(parts.splice(0, 1)[0]) : 1;
    let year = parseInt(parts.splice(0, 1)[0]);

    // Check date validity
    let date = new Date();

    // Check date validity
    date.setFullYear(year, month - 1, day);
    if (!((date.getFullYear() == year) && (date.getMonth() + 1 == month) && (date.getDate() == day))) {
      return null;
    }

    return {
      day: (expectedElementsCount == 3) ? day : null,
      month: (expectedElementsCount >= 2) ? month : null,
      year: year
    };
  }

  ngOnInit(): void {
    this.currentStepNumber = this.voteWorkflowViewComponent.getStepNumber(Step.Summary);
    this.nextStepNumber = this.voteWorkflowViewComponent.getStepNumber(Step.Control);
    this.birthDateScope = this.currentVotingService.birthDateScope;
    this.createForm();
    this.currentVotingService.lastVisitedPage = 'SUMMARY';
  }

  get hasMixedBallots(): boolean {
    return this.votations.length > 0 && this.elections.length > 0;
  }

  get hasSingleBallot(): boolean {
    return this.currentVotingService.hasSingleBallot();
  }

  get isGroupedVotation(): boolean {
    return this.currentVotingService.operation.groupVotation;
  }

  get votations(): Votation[] {
    return this.currentVotingService.votations;
  }

  get elections(): ElectionBallot[] {
    return this.currentVotingService.elections;
  }

  quit() {
    // TODO
  }

  back(): void {
    this.router.navigate(['../ballotsList'], {relativeTo: this.route});
  }

  canVote() {
    return this.nextStepChecked &&
           !this.busy &&
           this.authenticationForm.valid
  }

  submitVote() {
    this.busy = true;
    this.protocolService.submitBallot(this.extractDate(`${this.authenticationForm.controls.birthDate.value}`))
      .subscribe(() => {
        this.router.navigate(['../control'], {relativeTo: this.route});
        this.busy = false;
      }, errorProvider => {
        this.busy = false;
        errorProvider.onValidationError(/.*BirthDateCheckException/, error => this.displayErrorDialog(error))
      });
  }

  private displayErrorDialog(error) {
    let canRetry = error.globalErrors[0].messageParams[1] == "true";
    let data: any = {
      type: canRetry ? 'warning' : 'error',
      titleKey: 'summary.authentication.error-dialog.title',
      messageKey: `summary.authentication.error-dialog.${error.globalErrors[0].messageKey}`,
      messageParams: {
        count: error.globalErrors[0].messageParams[0]
      }
    };
    if (canRetry) {
      data.primaryButtonKey = 'global.actions.retry'
    } else {
      data.secondaryButtonKey = 'global.actions.quit'
    }

    this.dialog.open(ChvoteDialogComponent, {
      width: '500px',
      disableClose: true,
      data: data
    }).afterClosed().subscribe(() => {
      if (!canRetry) {
        this.quit();
      }
    });
  }
}

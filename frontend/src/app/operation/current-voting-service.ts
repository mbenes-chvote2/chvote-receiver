/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { Injectable } from '@angular/core';
import { Operation } from '../model/operation';
import { tap } from 'rxjs/operators';
import { Observable, of, ReplaySubject } from 'rxjs';
import { OperationService } from './operation.service';
import { VoterOperationData } from '../model/voter-operation-data';
import { ElectionBallot } from '../model/election';
import { Subject, Votation } from '../model/votation';
import { isVotationBallot } from '../model/ballot';
import { FinalizationLogData } from '../model/finalization-log-data';

type Page = 'IDENTIFICATION' | 'LEGAL_INFORMATION' | 'BALLOT_SELECTION' | 'SUMMARY' | 'CONTROL' | 'FINALIZATION';

export class CurrentVoting {
  operation: Operation;
  votations?: Votation[];
  elections?: ElectionBallot[];
  finalizationLog?: FinalizationLogData;
  birthDateScope: 'DAY' | 'MONTH' | 'YEAR';
  lastVisited: Page;

  constructor(operation: Operation) {
    this.operation = operation;
  }
}

/**
 * This class contains all elements concerning a current voting process.
 */
@Injectable()
export class CurrentVotingService {

  private _currentVoting: CurrentVoting;
  currentVotingChange = new ReplaySubject<CurrentVoting>(1);

  constructor(private operationService: OperationService) {
    let currentVotingJson = sessionStorage.getItem("current-voting");
    if (currentVotingJson) {
      this._currentVoting = JSON.parse(currentVotingJson);
    }
  }

  get operation() {
    return this._currentVoting.operation;
  }

  get votations(): Votation[] {
    return this._currentVoting.votations;
  }

  get birthDateScope() {
    return this._currentVoting.birthDateScope;
  }

  get elections() {
    return this._currentVoting.elections;
  }

  get finalizationLog(): FinalizationLogData {
    return this._currentVoting.finalizationLog;
  }

  get lastVisitedPage(): Page {
    return this._currentVoting.lastVisited;
  }

  set lastVisitedPage(page: Page) {
    this._currentVoting.lastVisited = page;
  }

  getOperationInstance(instanceId: string): Observable<Operation> {
    if (this._currentVoting && this._currentVoting.operation.protocolInstanceId === instanceId) {
      return of(this._currentVoting.operation);
    }

    return this.operationService.getOperation(instanceId).pipe(
      tap(operation => {
        this._currentVoting = new CurrentVoting(operation);
        this.notifyChange();
      })
    );
  }

  attachVoterOperationData(voterOperationData: VoterOperationData) {
    this._currentVoting.votations = voterOperationData.votations;
    this._currentVoting.elections = voterOperationData.elections;
    this._currentVoting.birthDateScope = voterOperationData.birthDateScope;
    this.notifyChange();
  }

  attachFinalizationLogData(finalizationLogData: FinalizationLogData) {
    this._currentVoting.finalizationLog = finalizationLogData;
    this.notifyChange();
  }

  private notifyChange() {
    sessionStorage.setItem("current-voting", JSON.stringify(this._currentVoting));
    this.currentVotingChange.next(this._currentVoting);
  }

  // TODO: rework this function to respect subjects repository ordering
  private static getAllSubjects(votations: Votation[]): Subject[] {
    let subjects: Subject[] = [];
    votations.forEach(votation => {
      votation.variantSubjects.forEach(variantSubject => {
        subjects = subjects.concat(variantSubject.subjects).concat(variantSubject.tieBreak);
      });
      subjects = subjects.concat(votation.subjects);
    });
    return subjects;
  }

  static initVotationBallot(votations: Votation[]): void {
    this.getAllSubjects(votations).forEach(subject => {
      subject.bufferedAnswer = subject.answers.find(answer => answer.virtual);
    });
  }

  static copyVotationBallotInBuffer(votations: Votation[]): void {
    this.getAllSubjects(votations).forEach(subject => {
      subject.bufferedAnswer = subject.selectedAnswer;
    });
  }

  static hasVotingBallotChanged(votations: Votation[]): boolean {
    for (let subject of this.getAllSubjects(votations)) {
      if ((subject.selectedAnswer === undefined && subject.bufferedAnswer !==
           subject.answers.find(answer => answer.virtual)) ||
          (subject.selectedAnswer !== undefined && subject.selectedAnswer !== subject.bufferedAnswer)) {
        return true
      }
    }
    return false;
  }

  static commitVotationBallot(votations: Votation[]): void {
    this.getAllSubjects(votations).forEach(subject => {
      subject.selectedAnswer = subject.bufferedAnswer;
      subject.bufferedAnswer = undefined;
    });
  }

  hasSingleBallot(): boolean {
    return (this.operation.groupVotation && this.elections.length === 0) ||
           (this.votations.length + this.elections.length === 1)
  }

  hasVotedFor(ballot: Votation | ElectionBallot) {
    if (isVotationBallot(ballot)) {
      return CurrentVotingService.getAllSubjects([ballot]).some(subject => subject.selectedAnswer !== undefined);
    }
    return ballot.elections.every(election =>
      election.candidates.some(candidate => candidate.isSelected)
    );
  }

  participationCount(): number {
    let counter = 0;
    for (let ballot of this.votations) {
      if (this.hasVotedFor(ballot)) {
        counter++;
      }
    }
    for (let ballot of this.elections) {
      if (this.hasVotedFor(ballot)) {
        counter++;
      }
    }
    return counter;
  }

  removeVoteFor(ballot: Votation | ElectionBallot) {
    if (isVotationBallot(ballot)) {
      CurrentVotingService.getAllSubjects([ballot]).forEach(subject => {
        subject.selectedAnswer = undefined
      });
    } else {
      ballot.elections.forEach(election =>
        election.candidates.forEach(candidate => candidate.isSelected = false)
      );
    }
    this.notifyChange();
  }

  buildSelection(): number[] {
    let selections = [];

    // TODO: add election selections
    // votation selections
    CurrentVotingService.getAllSubjects(this.votations).forEach(subject => {
      selections.push(subject.selectedAnswer.protocolIndex);
    });

    return selections;
  }

  populateVerificationCodes(verificationCodes: string[]): void {
    let index: number = 0;
    // TODO: populate elections
    CurrentVotingService.getAllSubjects(this.votations).forEach(subject => {
      subject.selectedAnswer.verificationCode = verificationCodes[index++];
    });
  }

  computeSubjectIndex(seekSubject: Subject): string {
    let ballotIndex: number = 0;
    for (let votation of this.votations) {
      ballotIndex++;
      let subjectIndex: number = 0;
      for (let subject of CurrentVotingService.getAllSubjects([votation])) {
        subjectIndex++;
        if (subject === seekSubject) {
          return `${ballotIndex}-${subjectIndex}`;
        }
      }
    }
    return 'unknown';
  }
}

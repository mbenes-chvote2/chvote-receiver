/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { CurrentVotingService } from './current-voting-service';
import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { ExternalConfigurationTranslateLoader } from '../core/ExternalConfigurationTranslateLoader';
import { catchError, take } from 'rxjs/operators';
import { HttpRequestErrorProvider } from '../core/http/ErrorInterceptor';

@Injectable()
export class OperationResolver implements Resolve<boolean> {

  constructor(private loader: ExternalConfigurationTranslateLoader,
              private injector: Injector,
              private currentVotingService: CurrentVotingService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    return this.currentVotingService.getOperationInstance(route.paramMap.get("operation"))
      .pipe(
        catchError((val: HttpRequestErrorProvider) => {
          val.onValidationError(/.*SiteIsClosedException/, error => {
            this.injector.get(Router)
              .navigate(
                ["error", route.paramMap.get("canton"), route.paramMap.get("operation"), route.paramMap.get("lang")],
                {
                  queryParams: {
                    errorKey: error.globalErrors[0].messageKey,
                    dateOpen: error.globalErrors[0].messageParams[0],
                    timeOpen: error.globalErrors[0].messageParams[1],
                    dateClose: error.globalErrors[0].messageParams[2],
                    timeClose: error.globalErrors[0].messageParams[3]
                  }
                });
          });
          return []
        }),
        take(1)
      )
  }

}

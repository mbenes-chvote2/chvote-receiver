/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { OperationComponent } from "./operation/operation.component";
import { Step } from './operation/vote-workflow-display/vote-workflow-view.component';
import { TranslationResolver } from './operation/translation.resolver';
import { IdentificationComponent } from './operation/1-identification/identification.component';
import { LegalInformationComponent } from './operation/2-legal-information/legal-information.component';
import { BallotsListComponent } from './operation/3-ballots-list/ballots-list.component';
import { SummaryComponent } from "./operation/4-summary/summary.component";
import { ControlComponent } from './operation/5-control/control.component';
import { FinalizationComponent } from './operation/6-finalization/finalization.component';
import { VotationBallotComponent } from './operation/3-ballots-list/votation-ballot/votation-ballot.component';
import { ErrorMessageComponent } from './core/error-message/error-message.component';
import { OperationResolver } from './operation/operation.resolver';

export const routes: Routes = [
  {
    path: "error/:canton/:operation/:lang",
    component: ErrorMessageComponent,
    resolve: {
      i18nReady: TranslationResolver
    }
  },
  {
    path: ':canton/:operation/:lang',
    component: OperationComponent,
    resolve: {
      i18nReady: TranslationResolver,
      operation: OperationResolver
    },
    children: [
      {
        path: 'identification',
        component: IdentificationComponent,
        data: {
          step: Step.Identification
        },
      },
      {
        path: 'legalInformation',
        component: LegalInformationComponent,
        data: {
          step: Step.LegalInformation
        }
      },
      {
        path: 'ballotsList',
        component: BallotsListComponent,
        data: {
          step: Step.BallotList
        }
      },
      {
        path: 'votationBallot',
        component: VotationBallotComponent,
        data: {
          step: Step.BallotList
        }
      },
      {
        path: 'votationBallot/:domainOfInfluence',
        component: VotationBallotComponent,
        data: {
          step: Step.BallotList
        }
      },
      {
        path: 'summary',
        component: SummaryComponent,
        data: {
          step: Step.Summary
        }
      },
      {
        path: 'control',
        component: ControlComponent,
        data: {
          step: Step.Control
        }
      },
      {
        path: 'finalization',
        component: FinalizationComponent,
        data: {
          step: Step.Finalization
        }
      },
      {
        path: '',
        redirectTo: "identification",
        pathMatch: "full"
      },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}

/*
 * #%L
 * chvote-receiver
 * %%
 * Copyright (C) 2016 - 2018 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

// spec.js
describe('chvote-receiver Demo App', function() {
    it('should have a title', function() {
        browser.ignoreSynchronization = true;
        browser.get('http://local:4200/');

        expect('CHVote Receiver').toEqual(browser.getTitle());
    });
});

# CHVote Vote Receiver - Front-end

# Global documentation
An overview of the project's structure is generated using `compodoc`.
The result is under `documentation` folder.

In order to update it, just run this command `npm run compodoc` and then open the browser at `http://127.0.0.1:8080`.

# Development guidelines

- [Coding style for FrontEnd development](https://gitlab.com/chvote2/documentation/chvote-docs/blob/master/development-directives/coding-style-FrontEnd.md)

## UI Components to use

* All Angular Material components 

## Global error handler

Unhandled JavaScript errors are catched by `GlobalErrorHandled` which :
* logs error to the browser console
* calls a REST endpoint of the server to centralize logs, in order to ease diagnostic. 

see `GlobalErrorHandler` in frontend project and `FrontEndLoggerController` in backend project

## Server error handling

HttpService must be used to call remote REST services, as it defines a global error handling strategy.

see `HttpService`

### Business errors

Business validation errors are received as HTTP responses with status code 400 and a ValidationErrorModel payload.

Calling component should declare an error handler to handle known business validation errors. To do so, one must set 'errorHandled' property to true.
```typescript
  retrieveOperations(): void {
    this.operationService.findAll().subscribe(
      operations => {
        // on success handler
      },
      err => {
        let error = err as ValidationErrorModel;
        if (error.exceptionClassName.indexOf("ImportOperationRepositoryException")) {
          // do something with business error

          // mark the error as being handled
          error.errorHandled=true;
        }
      }
    );
  }
```

Error interceptor will check that property to determine if business error has been handled by the calling component. If not, a generic business error popup will be shown.

see `BusinessErrorDialogComponent`

### Technical errors

Technical errors (typically RuntimeException) are received as HTTP responses with status code 500 and a TechnicalErrorVo payload.

Error interceptor will first display a technical error dialog box and then call component's error handler.

see `TechnicalErrorDialogComponent`

## I18N

### Selected component
Angular native I18N is still in beta and requires to bundle one application per language. It does not allow to change language on the fly.
 
We use [ngx-translate](https://github.com/ngx-translate/core) instead.

### Usage

* pipe : 
```json
{{ 'footer.content' | translate:footerParam }}
``` 

* service : 
```json
translate.get('HELLO', {value: 'world'}).subscribe((res: string) => { console.log(res); });
```

* directive : 
```html
 <div [translate]="'HELLO'" [translateParams]="{value: 'world'}"></div>
 ```

### Translation files
#### Location

Translation files are to be placed into assets/i18n/<lang>.json (ex: fr.json, de.json)

#### Structure

JSON translation files are to be structured according to application structure, i.e:

```json
 {
  "home":{
    "operationList":{
      "message": "Liste des opérations"
    }
  }
}
```



# Building

## Prerequisites

* Install NodeJS and NPM - https://nodejs.org/en/download/ 

Current version is `v10.13.0`

* Install Angular CLI plugin

Current version is `7.1.3`

```bash 
npm install -g @angular/cli
```

## Install required node modules.

This done by this command:

```bash
cd frontend
npm install
```

## Ejected configuration

In order to be able to do code coverage on e2e test it has been required to set Angular cli configuration as ejected.
Main impacts are :
- Execute Angular cli build related tasks are no longer available.
- Some npm tasks are not overridable hence you should never modify/rely on them
- There are new task created for our usage
- webpack.config.js should never be directly updated. One should use webpack.extra.config for e2e test or create a new one if needed
- webpack.config.js is in git ignore. 

# Running

## Pre-requisites

To run locally, the frontend application depends on the backend server.

To start the backend server, see ../backend/README.md

## Run command

```bash
cd frontend
npm start
```

Open the browser at: [http://localhost:4400/test/ge/fr](http://localhost:4400/test/ge/fr)

# Tips

## End to end tests

You must update webdriver before running end to end tests for the first time:

```
npm update-web-driver
```

You can run end to end tests locally using :

```bash
npm e2eLocal
```

You can select a subset of the specs to execute by editing ```specFilter``` in ```protractor.local.conf.js```


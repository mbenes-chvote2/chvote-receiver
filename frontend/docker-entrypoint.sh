#!/bin/sh

# replace the base href with BASE_URL environment variable if set, with / otherwise
sed -i "s#<base href=\"/test/vote/\">#<base href=\"${BASE_URL:-/}vote/\">#g" /usr/share/nginx/html/index.html
sed -i "s#<base href=\"/test/vote/\">#<base href=\"${BASE_URL:-/}test/vote/\">#g" /usr/share/nginx/test/html/index.html

sed -i "s#<meta name=\"apiBaseUrl\" content=\"http://localhost:8080/test/api\">#<meta name=\"apiBaseUrl\" content=\"${BASE_URL:-/}api\">#g" /usr/share/nginx/html/index.html
sed -i "s#<meta name=\"apiBaseUrl\" content=\"http://localhost:8080/test/api\">#<meta name=\"apiBaseUrl\" content=\"${BASE_URL:-/}test/api\">#g" /usr/share/nginx/test/html/index.html

nginx -g "daemon off;"

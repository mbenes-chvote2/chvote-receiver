function buildFrontEnd {
    echo "build frontend"
    cd frontend
    npm run build-prod
    cd ..
}

function buildFrontEndImage {
    echo "build frontend image"
    cd frontend
    docker build -t chvote/receiver-front .
    cd ..
}


function buildBackend {
    echo "build backend"
    cd backend
    mvn clean install
    cd ..
}

function buildBackendImage {
    echo "build backend image"
    cd backend/vr-backend-rest
    export BACKEND_ARTIFACT=`basename $(ls target/*.jar)`
    docker build -f Dockerfile --build-arg finalName=$BACKEND_ARTIFACT -t chvote/receiver-back .
    cd ../..
}

function buildMockServerImage {
    echo "build mock-server image"
    cd backend/vr-backend-mock-server/vr-backend-mock-server-rest
    export BACKEND_ARTIFACT=`basename $(ls target/*.jar)`
    docker build -f Dockerfile --build-arg finalName=$BACKEND_ARTIFACT -t chvote/receiver-mock .
    cd ../../..
}


function buildReverseProxyImage(){
    echo "reverse proxy image"
    cd receiver-docker/receiver-reverse-proxy/
    docker build -t chvote/receiver-reverse-proxy .
    cd ../../
}

function buildAll(){
    buildFrontEnd
    buildFrontEndImage
    buildReverseProxyImage
    buildBackend
    buildBackendImage
    buildMockServerImage
}

if [ $# -eq 0 ]
then
echo "./buildLocal.sh [steps]*"
echo "1) build FrontEnd"
echo "2) build FrontEnd Image"
echo "3) build ReverseProxy Image"
echo "4) build backend"
echo "5) build backend Image"
echo "6) build Mock Server Image"
echo "0) build All"
fi

for param in "$@"
do
    case $param in
       1) buildFrontEnd ;;
       2) buildFrontEndImage ;;
       3) buildReverseProxyImage ;;
       4) buildBackend ;;
       5) buildBackendImage ;;
       6) buildMockServerImage ;;
       0) buildAll ;;
       *) echo "INVALID CHOICE!" ;;
    esac
done

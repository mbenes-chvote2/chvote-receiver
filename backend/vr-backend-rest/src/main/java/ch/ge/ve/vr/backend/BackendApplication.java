/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend;

import ch.ge.ve.jacksonserializer.BigIntegerAsBase64Deserializer;
import ch.ge.ve.jacksonserializer.BigIntegerAsBase64Serializer;
import ch.ge.ve.jacksonserializer.JSDates;
import ch.ge.ve.vr.backend.rest.RejectAfterMaxAttempt;
import ch.ge.ve.vr.backend.rest.RestErrorHandler;
import ch.ge.ve.vr.backend.rest.config.CorsConfig;
import ch.ge.ve.vr.backend.rest.config.CustomHeaderFilter;
import ch.ge.ve.vr.backend.rest.config.RestConfig;
import ch.ge.ve.vr.backend.rest.config.Slf4jDataFilter;
import ch.ge.ve.vr.backend.rest.config.WebSecurityConfig;
import ch.ge.ve.vr.backend.service.config.VoteReceiverConfigurationProperties;
import ch.ge.ve.vr.backend.service.operation.PactOperationListener;
import ch.ge.ve.vr.backend.web.conf.ThymeleafDefaultConfiguration;
import java.security.Security;
import java.sql.SQLException;
import java.time.Clock;
import java.util.TimeZone;
import javax.annotation.PostConstruct;
import org.apache.http.client.fluent.Executor;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.h2.tools.Server;
import org.springframework.amqp.core.AnonymousQueue;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.retry.interceptor.RetryInterceptorBuilder;
import org.springframework.retry.interceptor.RetryOperationsInterceptor;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

/**
 * The main spring-boot class that bootstrap the backend application
 */

@EnableAsync
@EntityScan({"ch.ge.ve.vr.backend.repository.entity"})
@Import({CorsConfig.class, WebSecurityConfig.class, BackendServices.class, RestErrorHandler.class})
@EnableJpaRepositories(basePackages = "ch.ge.ve.vr.backend.repository")
@SpringBootApplication(scanBasePackages = {"ch.ge.ve.vr.backend.rest.controller", "ch.ge.ve.vr.backend.web"})
public class BackendApplication {
  private static final String                              VOTE_RECEIVER_EXCHANGE       = "vote-receiver-exchange";
  private static final String                              NEW_OPERATION_QUEUE_NAME     = "new-operation";
  private static final String                              DESTROY_OPERATION_QUEUE_NAME = "destroy-operation";
  private final        VoteReceiverConfigurationProperties voteReceiverConfigurationProperties;

  @Bean
  Clock defaultClock() {
    return Clock.systemDefaultZone();
  }

  @Autowired
  public BackendApplication(VoteReceiverConfigurationProperties voteReceiverConfigurationProperties) {
    this.voteReceiverConfigurationProperties = voteReceiverConfigurationProperties;
  }

  @PostConstruct
  public void setDefaultTimezone() {
    TimeZone.setDefault(TimeZone.getTimeZone(voteReceiverConfigurationProperties.getTimezone()));
  }

  @Bean
  public ServletRegistrationBean<DispatcherServlet> restAppServletRegistrationBean() {
    DispatcherServlet dispatcherServlet = new DispatcherServlet();
    AnnotationConfigWebApplicationContext applicationContext = new AnnotationConfigWebApplicationContext();
    applicationContext.register(RestConfig.class);
    dispatcherServlet.setApplicationContext(applicationContext);
    ServletRegistrationBean<DispatcherServlet>
        servletRegistrationBean = new ServletRegistrationBean<>(dispatcherServlet, "/api/*", "/test/api/*");
    servletRegistrationBean.setName("restAppServletRegistrationBean");
    return servletRegistrationBean;
  }

  @Bean
  public ServletRegistrationBean<DispatcherServlet> webAppServletRegistrationBean() {
    DispatcherServlet dispatcherServlet = new DispatcherServlet();
    AnnotationConfigWebApplicationContext applicationContext = new AnnotationConfigWebApplicationContext();
    applicationContext.register(ThymeleafDefaultConfiguration.class);
    dispatcherServlet.setApplicationContext(applicationContext);
    ServletRegistrationBean<DispatcherServlet> servletRegistrationBean =
        new ServletRegistrationBean<>(dispatcherServlet, "/*", "/test/*");
    servletRegistrationBean.setName("webAppServletRegistrationBean");
    return servletRegistrationBean;
  }


  @Bean
  TopicExchange exchange() {
    return new TopicExchange(VOTE_RECEIVER_EXCHANGE);
  }


  @Bean
  public Queue newOperationQueue() {
    return new AnonymousQueue();
  }

  @Bean
  public Queue deleteOperationQueue() {
    return new AnonymousQueue();
  }

  /**
   * Binder used to send message to the RabbitMq chanel on operation creation
   *
   * @param exchange the channel
   *
   * @return Binding
   */
  @Bean
  Binding newOperationBinding(TopicExchange exchange) {
    return BindingBuilder.bind(newOperationQueue())
                         .to(exchange)
                         .with("vote.receiver." + NEW_OPERATION_QUEUE_NAME);
  }

  /**
   * Binder used to send message to the RabbitMq chanel on operation desctruction
   *
   * @param exchange the channel
   *
   * @return Binding
   */
  @Bean
  Binding destroyOperationBinding(TopicExchange exchange) {
    return BindingBuilder.bind(deleteOperationQueue())
                         .to(exchange)
                         .with("vote.receiver." + DESTROY_OPERATION_QUEUE_NAME);
  }

  /**
   * Message listner used to comminucate with RabbitMq chanel
   *
   * @param connectionFactory:     Connection factory
   * @param pactOperationListener: Listner on the PACT side
   *
   * @return org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer
   */
  @Bean
  SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
                                           PactOperationListener pactOperationListener) {
    SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
    container.setConnectionFactory(connectionFactory);
    container.setQueues(newOperationQueue(), deleteOperationQueue());
    container.setAdviceChain(interceptor());
    MessageListenerAdapter messageListenerAdapter = new MessageListenerAdapter();
    messageListenerAdapter.setDelegate(pactOperationListener);
    messageListenerAdapter.addQueueOrTagToMethodName(newOperationQueue().getName(), "newOperation");
    messageListenerAdapter.addQueueOrTagToMethodName(deleteOperationQueue().getName(), "destroyOperation");
    container.setMessageListener(messageListenerAdapter);
    return container;
  }

  @Bean
  Executor httpExecutor() {
    return Executor.newInstance();
  }

  /**
   * Intercetor used to manage retries on authentication failure mechanism
   *
   * @return @{@link RetryOperationsInterceptor}
   */
  @Bean
  RetryOperationsInterceptor interceptor() {
    return RetryInterceptorBuilder.stateless()
                                  .maxAttempts(5)
                                  .backOffOptions(1000, 2., 10000)
                                  .recoverer(new RejectAfterMaxAttempt())
                                  .build();
  }

  /**
   * Spring template used to manage retries on authentication failure mechanism
   *
   * @return @{@link RetryTemplate}
   */
  @Bean
  RetryTemplate retryTemplate() {
    return new RetryTemplate();
  }

  @Bean
  public Jackson2ObjectMapperBuilderCustomizer jackson2ObjectMapperBuilderCustomizer() {
    return jacksonObjectMapperBuilder -> {
      jacksonObjectMapperBuilder.serializers(JSDates.SERIALIZER);
      jacksonObjectMapperBuilder.deserializers(JSDates.DESERIALIZER);
      jacksonObjectMapperBuilder.serializers(new BigIntegerAsBase64Serializer());
      jacksonObjectMapperBuilder.deserializers(new BigIntegerAsBase64Deserializer());
    };
  }

  /**
   * FIXME In production should have a profile that exclude this server creation
   *
   * @return the created server
   *
   * @throws SQLException if we cannot connect
   */
  @Bean(initMethod = "start", destroyMethod = "stop")
  @Profile("!test")
  public Server h2Server() throws SQLException {
    return Server.createTcpServer("-web", "-tcp", "-tcpAllowOthers", "-tcpPort", "57645");
  }


  /**
   * Initializes the Vote Receiver SpringBoot application.
   *
   * @param args arguments.
   */
  public static void main(String[] args) {
    Security.addProvider(new BouncyCastleProvider());
    new SpringApplicationBuilder(BackendApplication.class)
        .web(WebApplicationType.SERVLET)
        .sources(CorsConfig.class, CustomHeaderFilter.class, Slf4jDataFilter.class)
        .run(args);
  }
}

/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.rest.model;

import ch.ge.ve.protocol.model.BallotAndQuery;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Voter ballot information
 */
public class VoterBallotInformation {
  private final BallotAndQuery ballotAndQuery;
  private final BirthDate      birthDate;

  @JsonCreator
  public VoterBallotInformation(@JsonProperty("ballotAndQuery") BallotAndQuery ballotAndQuery,
                                @JsonProperty("birthDate") BirthDate birthDate) {
    this.ballotAndQuery = ballotAndQuery;
    this.birthDate = birthDate;
  }


  public BallotAndQuery getBallotAndQuery() {
    return ballotAndQuery;
  }

  public Integer getBirthDateDay() {
    return birthDate.day;
  }

  public Integer getBirthDateMonth() {
    return birthDate.month;
  }

  public Integer getBirthDateYear() {
    return birthDate.year;
  }

  private static class BirthDate {
    private final Integer day;
    private final Integer month;
    private final Integer year;

    @JsonCreator
    private BirthDate(@JsonProperty("day") Integer day,
                      @JsonProperty("month") Integer month,
                      @JsonProperty("year") Integer year) {
      this.day = day;
      this.month = month;
      this.year = year;
    }

  }


}

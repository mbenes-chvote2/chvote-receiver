/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.rest.controller;

import ch.ge.ve.vr.backend.repository.entity.RawFile;
import ch.ge.ve.vr.backend.rest.interceptor.CheckSiteIsOpen;
import ch.ge.ve.vr.backend.service.asset.AssetService;
import java.io.ByteArrayInputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller delivering asset
 */
@RestController
@RequestMapping("/asset")
public class AssetController {

  private final AssetService assetService;

  @Autowired
  public AssetController(AssetService assetService) {
    this.assetService = assetService;
  }

  @ResponseBody
  @GetMapping(path = "translations", produces = MediaType.APPLICATION_JSON_VALUE)
  public String getTranslations(@RequestParam("protocolInstanceId") String protocolInstanceId) {
    return assetService.getTranslations(protocolInstanceId);
  }

  @ResponseBody
  @GetMapping(path = "document", produces = MediaType.APPLICATION_PDF_VALUE)
  @CheckSiteIsOpen
  public ResponseEntity getPdfFile(
      @RequestParam("protocolInstanceId") String protocolInstanceId,
      @RequestParam("fileIndex") int fileIndex) {
    RawFile pdfFile = assetService.getFile(protocolInstanceId, fileIndex);
    return ResponseEntity.ok()
                         .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + pdfFile.getName() + ".pdf\"")
                         .body(new InputStreamResource(new ByteArrayInputStream(pdfFile.getContent())));
  }

  @ResponseBody
  @GetMapping(path = "coat-of-arms.svg", produces = "image/svg+xml")
  public ResponseEntity getCoatOfArms(@RequestParam("doiId") String doiId) {
    return ResponseEntity
        .ok()
        .body(new InputStreamResource(new ByteArrayInputStream(assetService.getDoiCoatOfArm(doiId))));
  }

}

/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.rest.interceptor;

import ch.ge.ve.chvote.pact.b2b.client.model.OperationType;
import ch.ge.ve.chvote.pact.b2b.client.model.VotingPeriod;
import ch.ge.ve.vr.backend.repository.data.Operation;
import ch.ge.ve.vr.backend.rest.exception.SiteIsClosedException;
import ch.ge.ve.vr.backend.service.exception.TechnicalException;
import ch.ge.ve.vr.backend.service.operation.OperationService;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * This interceptor use annotation @link CheckSiteIsOpen on a controller method.
 * If it is present it will catch parameter protocolId and verify that the site is open.
 * A site is open if current time is between site opening date and
 * site closing date + voting period
 */
@Component
public class CheckSiteIsOpenInterceptor extends HandlerInterceptorAdapter {

  private final OperationService operationService;

  @Autowired
  public CheckSiteIsOpenInterceptor(OperationService operationService) {
    this.operationService = operationService;
  }

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
    if (handler instanceof HandlerMethod) {
      HandlerMethod handlerMethod = (HandlerMethod) handler;
      CheckSiteIsOpen checkSiteIsOpen = handlerMethod.getMethodAnnotation(CheckSiteIsOpen.class);
      if (checkSiteIsOpen != null) {
        String protocolInstanceParameter = checkSiteIsOpen.value();
        String protocolId = request.getParameter(protocolInstanceParameter);
        if (StringUtils.isEmpty(protocolId)) {
          throw new TechnicalException(
              "Could not find parameter " + protocolInstanceParameter + " in request " + request.getRequestURI());
        }
        checkSiteIsOpen(protocolId, checkSiteIsOpen.allowedDuringGracePeriod());
      }
    }


    return super.preHandle(request, response, handler);
  }

  private void checkSiteIsOpen(String protocolId, boolean allowedDuringGracePeriod) {
    Operation operation = operationService.getOperationWithoutOperationCheckAccess(protocolId);
    if (operation.getType() != OperationType.TEST) {
      VotingPeriod votingPeriod = operation.getVotingPeriod();
      LocalDateTime openingDate = votingPeriod.getOpeningDate();
      LocalDateTime closingDate = votingPeriod.getClosingDate();

      if (openingDate.isAfter(LocalDateTime.now())) {
        throw new SiteIsClosedException("site-is-not-open", openingDate, closingDate);
      }

      if (closingDate.plus(allowedDuringGracePeriod ? votingPeriod.getGracePeriod() : 0, ChronoUnit.MINUTES)
                     .isBefore(LocalDateTime.now())) {
        throw new SiteIsClosedException("site-is-closed", openingDate, closingDate);
      }
    }

  }
}

/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.web.conf;

import java.util.Collection;
import java.util.Locale;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.util.CollectionUtils;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.thymeleaf.cache.StandardCacheManager;
import org.thymeleaf.dialect.IDialect;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

/**
 * Thymeleaf configuration
 */
@Configuration
@ComponentScan(basePackages = {"ch.ge.ve.vr.backend.web.controller"})
@ConditionalOnMissingBean(SpringTemplateEngine.class)
public class ThymeleafDefaultConfiguration {

  private final Collection<ITemplateResolver> templateResolvers;

  private final Collection<IDialect> dialects;

  public ThymeleafDefaultConfiguration(Collection<ITemplateResolver> templateResolvers,
                                       ObjectProvider<Collection<IDialect>> dialectsProvider) {
    this.templateResolvers = templateResolvers;
    this.dialects = dialectsProvider.getIfAvailable();
  }

  @Bean
  public SpringTemplateEngine templateEngine() {
    SpringTemplateEngine engine = new SpringTemplateEngine();
    for (ITemplateResolver templateResolver : this.templateResolvers) {
      engine.addTemplateResolver(templateResolver);
    }
    if (!CollectionUtils.isEmpty(this.dialects)) {
      for (IDialect dialect : this.dialects) {
        engine.addDialect(dialect);
      }
    }
    return engine;
  }

  @Bean
  public ThymeleafViewResolver thymeleafViewResolver() {
    ThymeleafViewResolver resolver = new ThymeleafViewResolver();
    resolver.setTemplateEngine(templateEngine());
    return resolver;
  }

  @Bean
  public StandardCacheManager thymeleafCacheManager() {
    StandardCacheManager cacheManager = new StandardCacheManager();
    cacheManager.setTemplateCacheMaxSize(500);
    return cacheManager;
  }

  @Bean
  public ClassLoaderTemplateResolver webPageTemplateResolver() {
    ClassLoaderTemplateResolver webPageTemplateResolver = new ClassLoaderTemplateResolver();
    webPageTemplateResolver.setPrefix("templates/");
    webPageTemplateResolver.setSuffix(".html");
    webPageTemplateResolver.setTemplateMode(TemplateMode.HTML);
    webPageTemplateResolver.setOrder(1);
    return webPageTemplateResolver;
  }

  @Bean
  public MessageSource messageSource() {
    ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
    messageSource.setBasename("classpath:/i18n/messages");
    return messageSource;
  }

  @Bean
  public LocaleResolver localeResolver() {
    SessionLocaleResolver sessionLocaleResolver = new SessionLocaleResolver();
    sessionLocaleResolver.setDefaultLocale(Locale.FRENCH);
    return sessionLocaleResolver;
  }

  @Bean
  public SessionLocaleResolver sessionLocaleResolver() {
    return new SessionLocaleResolver();
  }

}

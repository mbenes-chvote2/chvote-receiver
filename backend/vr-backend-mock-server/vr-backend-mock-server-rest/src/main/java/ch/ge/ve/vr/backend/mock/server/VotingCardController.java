/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.mock.server;

import ch.ge.ve.vr.backend.repository.VoterRepository;
import ch.ge.ve.vr.backend.repository.entity.VoterHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Transactional
public class VotingCardController {
  public enum BURNING_TYPE {
    AUTHENTICATION, CONFIRMATION, NONE
  }


  private final VoterRepository voterRepository;

  @Autowired
  public VotingCardController(VoterRepository voterRepository) {
    this.voterRepository = voterRepository;
  }


  @GetMapping(path = "voting-card/burn")
  public void burnVotingCard(
      @RequestParam(value = "voterIndex") int voterIndex,
      @RequestParam(value = "protocolId") String protocolId,
      @RequestParam(value = "burningType") BURNING_TYPE burningType) {
    VoterHolder voter = voterRepository.findByProtocolInstanceIdAndVoterId(protocolId, voterIndex)
                                       .orElseThrow(IllegalArgumentException::new);

    voter.setBirthDayCheckFailed(0);
    voter.setConfirmationCheckFailed(0);

    if (burningType == BURNING_TYPE.AUTHENTICATION) {
      voter.setBirthDayCheckFailed(5);
    }

    if (burningType == BURNING_TYPE.CONFIRMATION) {
      voter.setConfirmationCheckFailed(5);
    }

    voterRepository.save(voter);
  }

}

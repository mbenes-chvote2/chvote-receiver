/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.mock.server;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PactController {
  private final PactSimulatorService pactSimulatorService;

  public PactController(PactSimulatorService pactSimulatorService) {
    this.pactSimulatorService = pactSimulatorService;
  }

  @GetMapping(path = "pact/resend-operation")
  void forceResend(@RequestParam(value = "protocolInstanceId") String protocolInstanceId) {
    pactSimulatorService.sendNewOperation(protocolInstanceId);
  }

  @GetMapping(path = "pact/destroy-operation")
  void destroy(@RequestParam(value = "protocolInstanceId") String protocolInstanceId) {
    pactSimulatorService.sendDestroyOperation(protocolInstanceId);
  }

}

/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.mock.server;

import ch.ge.ve.chvote.pact.b2b.client.model.Voter;
import ch.ge.ve.protocol.core.support.PrimesCache;
import ch.ge.ve.protocol.model.ElectionSetWithPublicKey;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ProtocolInstance {

  private String                              protocolId;
  private List<EncryptionPublicKey>           publicKeys = new ArrayList<>();
  private Map<Integer, Voter>                 receiverVoters;
  private List<ch.ge.ve.protocol.model.Voter> protocolVoters;
  private PrimesCache                         primesCache;
  private ElectionSetWithPublicKey            electionSet;

  public ProtocolInstance(String protocolId) {
    this.protocolId = protocolId;
  }

  public String getProtocolId() {
    return protocolId;
  }

  public void setProtocolId(String protocolId) {
    this.protocolId = protocolId;
  }

  public List<EncryptionPublicKey> getPublicKeys() {
    return publicKeys;
  }

  public void setPublicKeys(List<EncryptionPublicKey> publicKeys) {
    this.publicKeys = publicKeys;
  }

  public Map<Integer, Voter> getReceiverVoters() {
    return receiverVoters;
  }

  public void setReceiverVoters(Map<Integer, Voter> receiverVoters) {
    this.receiverVoters = receiverVoters;
  }

  public List<ch.ge.ve.protocol.model.Voter> getProtocolVoters() {
    return protocolVoters;
  }

  public void setProtocolVoters(List<ch.ge.ve.protocol.model.Voter> protocolVoters) {
    this.protocolVoters = protocolVoters;
  }

  public PrimesCache getPrimesCache() {
    return primesCache;
  }

  public void setPrimesCache(PrimesCache primesCache) {
    this.primesCache = primesCache;
  }

  public ElectionSetWithPublicKey getElectionSet() {
    return electionSet;
  }

  public void setElectionSet(ElectionSetWithPublicKey electionSet) {
    this.electionSet = electionSet;
  }
}

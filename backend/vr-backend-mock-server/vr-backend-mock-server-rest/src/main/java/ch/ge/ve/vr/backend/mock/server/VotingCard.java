/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.mock.server;

import ch.ge.ve.vr.backend.service.model.BirthDateScope;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class VotingCard {
  private final int            voterId;
  private final String         cardNumber;
  private final String         confirmationCode;
  private final String         finalizationCode;
  private final BirthDateScope birthDateScope;
  private final List<String>   verificationCodes;

  @JsonCreator
  public VotingCard(@JsonProperty("voterId") int voterId,
                    @JsonProperty("cardNumber") String cardNumber,
                    @JsonProperty("confirmationCode") String confirmationCode,
                    @JsonProperty("finalizationCode") String finalizationCode,
                    @JsonProperty("verificationCodes") List<String> verificationCodes,
                    @JsonProperty("birthDateScope") BirthDateScope birthDateScope) {
    this.voterId = voterId;
    this.cardNumber = cardNumber;
    this.confirmationCode = confirmationCode;
    this.finalizationCode = finalizationCode;
    this.birthDateScope = birthDateScope;
    this.verificationCodes = verificationCodes;
  }

  public int getVoterId() {
    return voterId;
  }

  public String getCardNumber() {
    return cardNumber;
  }

  public String getConfirmationCode() {
    return confirmationCode;
  }

  public String getFinalizationCode() {
    return finalizationCode;
  }

  public List<String> getVerificationCodes() {
    return verificationCodes;
  }

  public BirthDateScope getBirthDateScope() {
    return birthDateScope;
  }
}

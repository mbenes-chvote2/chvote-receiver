/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.fixtures;

import ch.ge.ve.chvote.pact.b2b.client.model.Lang;
import ch.ge.ve.vr.backend.repository.data.BallotDocumentation;
import ch.ge.ve.vr.backend.repository.data.Election;
import java.io.IOException;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class ElectionBuilder {
  private final Set<FileReference>       rawFiles;
  private final AtomicInteger            protocolIndex;
  private       String                   domainOfInfluence = DoiIds.DOI_FED;
  private       Map<Lang, String>        localizedLabel    = new EnumMap<>(Lang.class);
  private       Set<BallotDocumentation> documentations    = new HashSet<>();


  public ElectionBuilder(Set<FileReference> rawFiles, AtomicInteger protocolIndex) {
    this.rawFiles = rawFiles;
    this.protocolIndex = protocolIndex;
  }

  public ElectionBuilder documentation(String label, Lang lang) {
    int index = rawFiles.size();
    try {
      rawFiles.add(new FileReference(this.getClass().getResourceAsStream("/test.pdf"), index, "test.pdf"));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    this.documentations.add(new BallotDocumentation(label, lang, index));
    return this;
  }

  public ElectionBuilder localizedLabel(Lang lang, String label) {
    this.localizedLabel.put(lang, label);
    return this;
  }

  public ElectionBuilder domainOfInfluence(String domainOfInfluence) {
    this.domainOfInfluence = domainOfInfluence;
    return this;
  }

  public Election build() {
    return new Election(domainOfInfluence, localizedLabel, documentations);
  }
}

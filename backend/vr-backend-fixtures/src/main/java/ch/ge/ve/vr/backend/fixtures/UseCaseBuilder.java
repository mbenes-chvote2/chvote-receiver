/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.fixtures;

import ch.ge.ve.chvote.pact.b2b.client.model.DocumentationType;
import ch.ge.ve.chvote.pact.b2b.client.model.Lang;
import ch.ge.ve.chvote.pact.b2b.client.model.OperationType;
import ch.ge.ve.chvote.pact.b2b.client.model.VotingPeriod;
import ch.ge.ve.vr.backend.repository.data.Documentation;
import ch.ge.ve.vr.backend.repository.data.Election;
import ch.ge.ve.vr.backend.repository.data.HighlightedQuestion;
import ch.ge.ve.vr.backend.repository.data.Operation;
import ch.ge.ve.vr.backend.repository.data.Votation;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;


public class UseCaseBuilder {
  private String             label          =
      "Opération virtuelle " + LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME);
  private String             canton         = "GE";
  private OperationType      type           = OperationType.TEST;
  private String             simulationName;
  private Set<Documentation> documentations = new HashSet<>();
  private boolean            groupVotation;
  private Lang               defaultLang    = Lang.FR;
  private VotingPeriod       votingPeriod;
  private LocalDateTime      date;

  private Set<FileReference>        rawFiles             = new HashSet<>();
  private List<Votation>            votations            = new ArrayList<>();
  private List<Election>            elections            = new ArrayList<>();
  private AtomicInteger             protocolIndex        = new AtomicInteger(1);
  private List<HighlightedQuestion> highlightedQuestions = new ArrayList<>();

  public UseCaseBuilder label(String label) {
    this.label = label;
    return this;
  }

  public UseCaseBuilder votation(Consumer<VotationBuilder> factory) {
    VotationBuilder builder = new VotationBuilder(rawFiles, protocolIndex);
    factory.accept(builder);
    votations.add(builder.build());
    return this;
  }

  public UseCaseBuilder election(Consumer<ElectionBuilder> factory) {
    ElectionBuilder builder = new ElectionBuilder(rawFiles, protocolIndex);
    factory.accept(builder);
    elections.add(builder.build());
    return this;
  }


  public UseCaseBuilder highlightedQuestion(String question, Lang lang) {
    int index = rawFiles.size();
    try {
      rawFiles.add(new FileReference(this.getClass().getResourceAsStream("/test.pdf"), index, "test.pdf"));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    this.highlightedQuestions.add(new HighlightedQuestion(question, lang, index));
    return this;
  }

  public UseCaseBuilder documentation(DocumentationType type, String label, Lang lang) {
    return documentation(type, label, lang, "test.pdf");
  }

  public UseCaseBuilder documentation(DocumentationType type, String label, Lang lang, String file) {
    int index = rawFiles.size();
    try {
      rawFiles.add(new FileReference(this.getClass().getResourceAsStream(String.format("/%s", file)), index, file));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    this.documentations.add(new Documentation(type, label, lang, index));
    return this;
  }

  public UseCaseBuilder groupVotation(boolean groupVotation) {
    this.groupVotation = groupVotation;
    return this;
  }

  public UseCaseBuilder defaultLang(Lang defaultLang) {
    this.defaultLang = defaultLang;
    return this;
  }

  public UseCaseBuilder votingPeriod(VotingPeriod votingPeriod) {
    this.votingPeriod = votingPeriod;
    return this;
  }

  public UseCaseBuilder date(LocalDateTime date) {
    this.date = date;
    return this;
  }

  public UseCaseBuilder canton(String canton) {
    this.canton = canton;
    return this;
  }


  public UseCaseBuilder forTest() {
    this.type = OperationType.TEST;
    return this;
  }

  public UseCaseBuilder forSimulation() {
    return forSimulation("Simulation name");
  }

  public UseCaseBuilder forSimulation(String simulationName) {
    this.type = OperationType.SIMULATION;
    this.simulationName = simulationName;
    return this;
  }

  public UseCaseBuilder forReal() {
    this.type = OperationType.REAL;
    return this;
  }

  public OperationInstance build() {
    int index = rawFiles.size();
    try {
      rawFiles
          .add(new FileReference(this.getClass().getResourceAsStream("/translation.json"), index, "translation.json"));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    Operation operation =
        new Operation(canton, "protocolInstanceId", type, label,
                      simulationName, defaultLang, groupVotation,
                      index, votingPeriod, date, documentations, highlightedQuestions);
    return new OperationInstance(operation, votations, elections, rawFiles);
  }

}

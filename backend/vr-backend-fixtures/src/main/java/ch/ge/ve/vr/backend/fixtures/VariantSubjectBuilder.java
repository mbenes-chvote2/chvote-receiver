/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.fixtures;

import ch.ge.ve.chvote.pact.b2b.client.model.Lang;
import ch.ge.ve.vr.backend.repository.data.Subject;
import ch.ge.ve.vr.backend.repository.data.VariantSubjects;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

class VariantSubjectBuilder {
  private final AtomicInteger         protocolIndex;
  private       EnumMap<Lang, String> localizedGroupLabel = new EnumMap<>(Lang.class);
  private       List<Subject>         subjects            = new ArrayList<>();
  private       Subject               tieBreak;

  VariantSubjectBuilder(AtomicInteger protocolIndex) {
    this.protocolIndex = protocolIndex;
  }

  VariantSubjectBuilder localizedGroupLabel(Lang lang, String label) {
    this.localizedGroupLabel.put(lang, label);
    return this;
  }

  VariantSubjectBuilder subject(Consumer<SubjectBuilder> factory) {
    SubjectBuilder subjectBuilder = new SubjectBuilder(protocolIndex);
    factory.accept(subjectBuilder);
    subjects.add(subjectBuilder.build());
    return this;
  }

  VariantSubjectBuilder tieBreak(Consumer<SubjectBuilder> factory) {
    SubjectBuilder subjectBuilder = new SubjectBuilder(protocolIndex);
    factory.accept(subjectBuilder);
    tieBreak = subjectBuilder.build();
    return this;
  }

  VariantSubjects build() {
    return new VariantSubjects(localizedGroupLabel, subjects, tieBreak);
  }
}

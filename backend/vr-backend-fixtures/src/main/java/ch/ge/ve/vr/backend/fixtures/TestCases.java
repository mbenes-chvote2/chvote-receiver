/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.fixtures;

import static ch.ge.ve.chvote.pact.b2b.client.model.DocumentationType.CERTIFICATES;
import static ch.ge.ve.chvote.pact.b2b.client.model.DocumentationType.FAQ;
import static ch.ge.ve.chvote.pact.b2b.client.model.DocumentationType.HIGHLIGHTED_QUESTION;
import static ch.ge.ve.chvote.pact.b2b.client.model.DocumentationType.TERMS;
import static ch.ge.ve.chvote.pact.b2b.client.model.Lang.DE;
import static ch.ge.ve.chvote.pact.b2b.client.model.Lang.FR;
import static ch.ge.ve.chvote.pact.b2b.client.model.Lang.IT;
import static ch.ge.ve.chvote.pact.b2b.client.model.Lang.RM;
import static ch.ge.ve.vr.backend.fixtures.DoiIds.DOI_BE;
import static ch.ge.ve.vr.backend.fixtures.DoiIds.DOI_FED;
import static ch.ge.ve.vr.backend.fixtures.DoiIds.DOI_GE;
import static ch.ge.ve.vr.backend.fixtures.DoiIds.DOI_VGE;

import ch.ge.ve.chvote.pact.b2b.client.model.OperationType;
import ch.ge.ve.chvote.pact.b2b.client.model.VotingPeriod;
import java.time.LocalDateTime;
import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * Utility methods to create some use cases for test
 */
public final class TestCases {

  private static final String SUBJECT_1 =
      "Acceptez-vous l'arrêté fédéral du 14 mars 2017 sur la <b>sécurité alimentaire</b>? (Contre-projet direct à " +
      "l'initiative populaire «Pour la sécurité alimentaire», qui a été retirée)";
  private static final String SUBJECT_2 =
      "Acceptez-vous l'arrêté fédéral du 17 mars 2017 sur le <b>financement additionnel de l'AVS par le biais d'un " +
      "relèvement de la taxe sur la valeur ajoutée</b>?";

  private static final String SUBJECT_IN =
      "Acceptez-vous l'initiative communale IN-5 «Pour des Fêtes de Genève plus courtes et plus conviviales»?";
  private static final String SUBJECT_CP =
      "Acceptez-vous le contreprojet à l'initiative communale IN-5 «Pour des Fêtes de Genève plus courtes et plus " +
      "conviviales»?";
  private static final String SUBJECT_QS =
      "<u>Question subsidiaire</u> : Si l'initiative communale IN-5 «Pour des Fêtes de Genève plus courtes et plus " +
      "conviviales» et le contreprojet sont acceptés, lequel des deux a-t-il votre préférence?";
  private static final String DE_PREFIX  = "[DE] - ";
  private static final String IT_PREFIX  = "[IT] - ";
  private static final String RM_PREFIX  = "[RM] - ";
  private static final String GROUP_NAME = "Fêtes de Genève";

  private TestCases() {
    // To prevent instantiation
  }

  /**
   * Creates a simple operation with predefined ballots.
   *
   * @return an operation instance
   */
  public static OperationInstance createSimpleOperation() {
    return createSimpleOperation(OperationType.TEST, "GE", true, false, false, null, null);
  }

  /**
   * Creates a simple operation with predefined ballots.
   *
   * @param type         the operation's type
   * @param votingPeriod the voting period
   * @param date         the operation's date
   *
   * @return an operation instance
   */
  public static OperationInstance createSimpleOperation(OperationType type,
                                                        VotingPeriod votingPeriod,
                                                        LocalDateTime date) {
    return createSimpleOperation(type, "GE", true, false, false, votingPeriod, date);
  }

  /**
   * Creates a simple operation with predefined ballots.
   *
   * @param type         the operation's type
   * @param canton       the operation's canton
   * @param withVotation true to add votation subjects
   * @param withElection true to add elections
   * @param grouped      true to mahe the votations grouped
   * @param votingPeriod the voting period
   * @param date         the operation's date
   *
   * @return an operation instance
   */
  public static OperationInstance createSimpleOperation(OperationType type,
                                                        String canton,
                                                        boolean withVotation,
                                                        boolean withElection,
                                                        boolean grouped,
                                                        VotingPeriod votingPeriod,
                                                        LocalDateTime date) {
    UseCaseBuilder useCaseBuilder = new UseCaseBuilder();

    useCaseBuilder.canton(canton)
                  .groupVotation(grouped)
                  .votingPeriod(votingPeriod)
                  .date(date);

    if (OperationType.REAL.equals(type)) {
      useCaseBuilder.forReal();
    } else if (OperationType.SIMULATION.equals(type)) {
      useCaseBuilder.forSimulation();
    } else {
      useCaseBuilder.forTest();
    }

    useCaseBuilder.defaultLang(("GE".equals(canton) || "VD".equals(canton)) ? FR : DE);

    String[] doiIds = getDOIsByCanton(canton);

    addPredefinedDocumentations(useCaseBuilder);

    if (withVotation) {
      addPredefinedVotations(useCaseBuilder, doiIds);
    }

    if (withElection) {
      addPredefinedElections(useCaseBuilder, doiIds);
    }

    return useCaseBuilder.build();
  }

  private static String[] getDOIsByCanton(String canton) {

    if ("GE".equals(canton)) {
      return new String[]{DOI_FED, DOI_GE, DOI_VGE};
    }

    if ("BE".equals(canton)) {
      return new String[]{DOI_FED, DOI_BE};
    }

    return new String[]{DOI_FED};
  }


  private static void addPredefinedDocumentations(UseCaseBuilder useCaseBuilder) {
    useCaseBuilder.documentation(CERTIFICATES, "Certificat", FR, "certificate-fr.pdf")
                  .documentation(CERTIFICATES, "Certificat", DE, "certificate-de.pdf")
                  .documentation(FAQ, "FAQ", FR, "faq-fr.pdf")
                  .documentation(FAQ, "FAQ [DE]", DE, "faq-de.pdf")
                  .documentation(TERMS, "Conditions d'utilisation", FR, "conditions_utilisation-fr.pdf")
                  .documentation(TERMS, "Conditions d'utilisation [DE]", DE, "conditions_utilisation-de.pdf")
                  .documentation(TERMS, "Conditions d'utilisation [IT]", IT, "conditions_utilisation-it.pdf")
                  .documentation(HIGHLIGHTED_QUESTION, "une question", FR)
                  .documentation(HIGHLIGHTED_QUESTION, "une autre question", FR)
                  .highlightedQuestion("Comment être certain-e que mon vote a bien été enregistré ?", FR);
  }

  private static void addPredefinedVotations(UseCaseBuilder useCaseBuilder, String... doiIds) {
    Stream.of(doiIds).forEach(
        doiId ->
            useCaseBuilder.votation(
                votation -> {
                  votation
                      .domainOfInfluence(doiId)
                      .documentation("Explications du conseil fédéral", FR)
                      .documentation("Explications du conseil cantonal", FR)
                      .documentation("Erläuterungen des Bundesrates", DE)
                      .documentation("Explicaziuns dal Cussegl federal 1", RM)
                      .documentation("Explicaziuns dal Cussegl federal 2", RM)
                      .documentation("Explicaziuns dal Cussegl federal 3", RM);
                  addVotationLocalizedLabel(votation, doiId);
                  if (DOI_FED.equals(doiId)) {
                    votation.variantSubjects(addVariantSubject());
                    votation.subject(addSubjectByDOI("2.", doiId, SUBJECT_1));
                  } else {
                    votation.subject(addSubjectByDOI("1.", doiId, SUBJECT_1))
                            .subject(addSubjectByDOI("2.", doiId, SUBJECT_2));
                  }
                }
            )
    );
  }

  private static void addPredefinedElections(UseCaseBuilder useCaseBuilder, String... doiIds) {
    Stream.of(doiIds).forEach(
        doiId ->
            useCaseBuilder.election(
                election -> election
                    .domainOfInfluence(doiId)
                    .localizedLabel(FR, "Election for " + doiId)
            )
    );
  }

  private static void addVotationLocalizedLabel(VotationBuilder votationBuilder, String domainOfInfluence) {
    if (DOI_FED.equals(domainOfInfluence)) {
      votationBuilder.localizedLabel(FR, "votation fédérale")
                     .localizedLabel(DE, "eidgenössische abstimmung")
                     .localizedLabel(IT, "votazione federale")
                     .localizedLabel(RM, "votaziun federala");
    } else if (DOI_GE.equals(domainOfInfluence) || DOI_BE.equals(domainOfInfluence)) {
      votationBuilder.localizedLabel(FR, "votation cantonale")
                     .localizedLabel(DE, "kantonale abstimmung")
                     .localizedLabel(IT, "votazione cantonale")
                     .localizedLabel(RM, "votaziun chantunala");
    } else {
      votationBuilder.localizedLabel(FR, "votation communale")
                     .localizedLabel(DE, "kommunale abstimmung")
                     .localizedLabel(IT, "votazione comunale")
                     .localizedLabel(RM, "votaziun communala");
    }
  }

  private static Consumer<VariantSubjectBuilder> addVariantSubject() {
    return variantSubject -> variantSubject.localizedGroupLabel(FR, GROUP_NAME)
                                           .localizedGroupLabel(DE, DE_PREFIX + GROUP_NAME)
                                           .localizedGroupLabel(IT, IT_PREFIX + GROUP_NAME)
                                           .localizedGroupLabel(RM, RM_PREFIX + GROUP_NAME)
                                           .subject(subject -> subject.localizedLabel(FR, SUBJECT_IN)
                                                                      .localizedLabel(DE, DE_PREFIX + SUBJECT_IN)
                                                                      .localizedLabel(IT, IT_PREFIX + SUBJECT_IN)
                                                                      .localizedLabel(RM, RM_PREFIX + SUBJECT_IN)
                                                                      .number("1.a")
                                                                      .answer(answer -> addAnswerLabelByDOI(answer,
                                                                                                            DoiIds.DOI_FED,
                                                                                                            AnswerType.YES))
                                                                      .answer(answer -> addAnswerLabelByDOI(answer,
                                                                                                            DoiIds.DOI_FED,
                                                                                                            AnswerType.NO))
                                                                      .answer(answer -> addAnswerLabelByDOI(answer,
                                                                                                            DoiIds.DOI_FED,
                                                                                                            AnswerType.BLANK)))
                                           .subject(subject -> subject.localizedLabel(FR, SUBJECT_CP)
                                                                      .localizedLabel(DE, DE_PREFIX + SUBJECT_CP)
                                                                      .localizedLabel(IT, IT_PREFIX + SUBJECT_CP)
                                                                      .localizedLabel(RM, RM_PREFIX + SUBJECT_CP)
                                                                      .number("1.b")
                                                                      .answer(answer -> addAnswerLabelByDOI(answer,
                                                                                                            DoiIds.DOI_FED,
                                                                                                            AnswerType.YES))
                                                                      .answer(answer -> addAnswerLabelByDOI(answer,
                                                                                                            DoiIds.DOI_FED,
                                                                                                            AnswerType.NO))
                                                                      .answer(answer -> addAnswerLabelByDOI(answer,
                                                                                                            DoiIds.DOI_FED,
                                                                                                            AnswerType.BLANK)))
                                           .tieBreak(tieBreak -> tieBreak.localizedLabel(FR, SUBJECT_QS)
                                                                         .localizedLabel(DE, DE_PREFIX + SUBJECT_QS)
                                                                         .localizedLabel(IT, IT_PREFIX + SUBJECT_QS)
                                                                         .localizedLabel(RM, RM_PREFIX + SUBJECT_QS)
                                                                         .number("1.c")
                                                                         .answer(answer -> addAnswerLabelByDOI(answer,
                                                                                                               DoiIds.DOI_FED,
                                                                                                               AnswerType.IN))
                                                                         .answer(answer -> addAnswerLabelByDOI(answer,
                                                                                                               DoiIds.DOI_FED,
                                                                                                               AnswerType.CP))
                                                                         .answer(answer -> addAnswerLabelByDOI(answer,
                                                                                                               DoiIds.DOI_FED,
                                                                                                               AnswerType.BLANK)));
  }

  private static Consumer<SubjectBuilder> addSubjectByDOI(String number, String doiId, String label) {
    return subject -> {
      addSubjectLabelByDOI(subject, doiId, label);
      subject
          .number(number)
          .answer(answer -> addAnswerLabelByDOI(answer, doiId, AnswerType.YES))
          .answer(answer -> addAnswerLabelByDOI(answer, doiId, AnswerType.NO))
          .answer(answer -> addAnswerLabelByDOI(answer, doiId, AnswerType.BLANK));
    };
  }

  private enum AnswerType {
    YES, NO, BLANK, IN, CP
  }

  private static void addSubjectLabelByDOI(SubjectBuilder subjectBuilder, String doiId, String label) {
    if (DOI_FED.equals(doiId)) {
      subjectBuilder.localizedLabel(FR, label + doiId)
                    .localizedLabel(DE, DE_PREFIX + label + " " + doiId)
                    .localizedLabel(IT, IT_PREFIX + label + " " + doiId)
                    .localizedLabel(RM, RM_PREFIX + label + " " + doiId);
    } else if (DOI_BE.equals(doiId)) {
      subjectBuilder.localizedLabel(DE,
                                    DE_PREFIX + label + " " + doiId +
                                    "<br/>" + "[FR] - " + label + " " + doiId);
    } else {
      subjectBuilder.localizedLabel(FR, label + " " + doiId);
    }
  }

  private static void addAnswerLabelByDOI(AnswerBuilder answerBuilder, String doiId, AnswerType answerType) {
    switch (answerType) {
      case YES:
        if (DOI_FED.equals(doiId)) {
          answerBuilder.localizedLabel(FR, "oui")
                       .localizedLabel(DE, "ja")
                       .localizedLabel(IT, "sì")
                       .localizedLabel(RM, "gea");
        } else if (DOI_BE.equals(doiId)) {
          answerBuilder.localizedLabel(DE, "ja<br/>oui");
        } else {
          answerBuilder.localizedLabel(FR, "oui");
        }
        break;
      case NO:
        if (DOI_FED.equals(doiId)) {
          answerBuilder.localizedLabel(FR, "non")
                       .localizedLabel(DE, "nein")
                       .localizedLabel(IT, "no")
                       .localizedLabel(RM, "na");
        } else if (DOI_BE.equals(doiId)) {
          answerBuilder.localizedLabel(DE, "nein<br/>non");
        } else {
          answerBuilder.localizedLabel(FR, "non");
        }
        break;
      case BLANK:
        answerBuilder.virtual(true);
        if (DOI_FED.equals(doiId)) {
          answerBuilder.localizedLabel(FR, "blanc")
                       .localizedLabel(DE, "leer")
                       .localizedLabel(IT, "s.o.")
                       .localizedLabel(RM, "l.v.");
        } else if (DOI_BE.equals(doiId)) {
          answerBuilder.localizedLabel(DE, "leer<br/>blanc");
        } else {
          answerBuilder.localizedLabel(FR, "blanc");
        }
        break;
      case IN:
        answerBuilder.localizedLabel(FR, "in")
                     .localizedLabel(DE, "vi")
                     .localizedLabel(IT, "in")
                     .localizedLabel(RM, "in");
        break;
      case CP:
        answerBuilder.localizedLabel(FR, "cp")
                     .localizedLabel(DE, "gge")
                     .localizedLabel(IT, "cp")
                     .localizedLabel(RM, "cp");
        break;
    }
  }

}

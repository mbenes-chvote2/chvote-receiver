/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.service.versionning

import ch.ge.ve.vr.backend.repository.OperationRepository
import ch.ge.ve.vr.backend.repository.entity.OperationHolder
import ch.ge.ve.vr.backend.service.config.VoteReceiverConfigurationProperties
import ch.ge.ve.vr.backend.service.operation.OperationImportService
import spock.lang.Specification

class ModelsVersionUpdaterTest extends Specification {

  def operationRepository = Mock(OperationRepository)
  def config = Mock(VoteReceiverConfigurationProperties)
  def importService = Mock(OperationImportService)
  def modelsVersionUpdater = new ModelsVersionUpdater(operationRepository, config, importService)

  def "updateOperationModels should update operations currently in database"() {
    given:
    config.modelVersion >> 1

    operationRepository.findOperationsForUpdate(1) >> [
            operation('instanceId1'),
            operation('instanceId2'),
            operation('instanceId2')
    ]

    when:
    modelsVersionUpdater.updateOperationModels()

    then:
    1 * importService.importOperation('instanceId1')
    1 * importService.importOperation('instanceId2')
  }

  def operation(String protocolInstanceId) {
    OperationHolder operation = new OperationHolder()
    operation.setProtocolInstanceId(protocolInstanceId)
    return operation
  }

}

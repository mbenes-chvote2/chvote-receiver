/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.service.operation

import ch.ge.ve.chvote.pact.b2b.client.PactB2BClient
import ch.ge.ve.chvote.pact.b2b.client.model.Attachment
import ch.ge.ve.chvote.pact.b2b.client.model.Canton
import ch.ge.ve.chvote.pact.b2b.client.model.Documentation
import ch.ge.ve.chvote.pact.b2b.client.model.DocumentationType
import ch.ge.ve.chvote.pact.b2b.client.model.Lang
import ch.ge.ve.chvote.pact.b2b.client.model.OperationBaseConfiguration
import ch.ge.ve.chvote.pact.b2b.client.model.OperationType
import ch.ge.ve.chvote.pact.b2b.client.model.VotationBallotConfig
import ch.ge.ve.chvote.pact.b2b.client.model.Voter
import ch.ge.ve.chvote.pact.b2b.client.model.VotingPeriod
import ch.ge.ve.chvote.pact.b2b.client.model.VotingSiteConfiguration
import ch.ge.ve.protocol.model.Candidate
import ch.ge.ve.protocol.model.DomainOfInfluence
import ch.ge.ve.protocol.model.Election
import ch.ge.ve.protocol.model.ElectionSetForVerification
import ch.ge.ve.vr.backend.repository.OperationRepository
import ch.ge.ve.vr.backend.repository.VoterRepository
import ch.ge.ve.vr.backend.repository.entity.OperationHolder
import ch.ge.ve.vr.backend.repository.entity.VoterHolder
import ch.ge.ve.vr.backend.service.TestHelpers
import ch.ge.ve.vr.backend.service.config.VoteReceiverConfigurationProperties
import ch.ge.ve.vr.backend.service.protocol.ProtocolService
import ch.ge.ve.vr.backend.service.voter.VoterService
import java.time.LocalDateTime
import org.skyscreamer.jsonassert.JSONAssert
import org.skyscreamer.jsonassert.JSONCompareMode
import spock.lang.Specification

class OperationImportServiceTest extends Specification {

  def operationRepository = Mock(OperationRepository)
  def voterRepository = Mock(VoterRepository)
  def config = new VoteReceiverConfigurationProperties()
  def pactClient = Mock(PactB2BClient)
  def protocolService = Mock(ProtocolService)
  def voterService = Mock(VoterService)

  def openingDate = LocalDateTime.of(2018, 8, 27, 12, 0)
  def closingDate = LocalDateTime.of(2018, 9, 22, 12, 0)
  def operationDate = LocalDateTime.of(2018, 9, 23, 0, 0)

  OperationImportService importService = new OperationImportService(
          operationRepository,
          voterRepository,
          config,
          TestHelpers.objectMapper,
          pactClient,
          protocolService,
          voterService
  )

  void setup() {
    def votation = attachment("/votationRepository.xml")
    def translations = attachment("/translation.json")
    def pdfFile = attachment("/test.pdf")
    def attachments = [(votation): 1L, (translations): 2L, (pdfFile): 3L]
    pactClient.getAttachment(_ as String, 1L) >> votation
    pactClient.getAttachment(_ as String, 2L) >> translations
    pactClient.getAttachment(_ as String, 3L) >> pdfFile

    config.modelVersion = 2

    protocolService.getElectionSet(_) >> new ElectionSetForVerification(
            [
                    new Election('VP', 27, 9, null)
            ],
            [
                    new Candidate('FED_CH_Q1:YES'),
                    new Candidate('FED_CH_Q1:NO'),
                    new Candidate('FED_CH_Q1:BLANK'),
                    new Candidate('FED_CH_Q2:YES'),
                    new Candidate('FED_CH_Q2:NO'),
                    new Candidate('FED_CH_Q2:BLANK'),
                    new Candidate('FED_CH_Q3:YES'),
                    new Candidate('FED_CH_Q3:NO'),
                    new Candidate('FED_CH_Q3:BLANK'),
                    new Candidate('CAN_GE_Q1:YES'),
                    new Candidate('CAN_GE_Q1:NO'),
                    new Candidate('CAN_GE_Q1:BLANK'),
                    new Candidate('COM_6608_Q1:YES'),
                    new Candidate('COM_6608_Q1:NO'),
                    new Candidate('COM_6608_Q1:BLANK'),
                    new Candidate('COM_6621_Q1:YES'),
                    new Candidate('COM_6621_Q1:NO'),
                    new Candidate('COM_6621_Q1:BLANK'),
                    new Candidate('COM_6621_Q2:YES'),
                    new Candidate('COM_6621_Q2:NO'),
                    new Candidate('COM_6621_Q2:BLANK'),
                    new Candidate('COM_6621_Q3:YES'),
                    new Candidate('COM_6621_Q3:NO'),
                    new Candidate('COM_6621_Q3:BLANK'),
                    new Candidate('COM_6621_Q4:YES'),
                    new Candidate('COM_6621_Q4:NO'),
                    new Candidate('COM_6621_Q4:BLANK')
            ],
            [],
            0,
            0,
            [] as int[]
    )

    pactClient.getOperationConfiguration(_) >> new OperationBaseConfiguration(
            'OPE', 'Operation', Canton.GE, 'Votation Populaire', '', Lang.FR, OperationType.TEST,
            new VotingPeriod(openingDate, closingDate, 9),
            operationDate, [attachments.get(votation)], []
    )

    pactClient.getVotingSiteConfiguration(_) >> new VotingSiteConfiguration(
            [
                    new Documentation(DocumentationType.CERTIFICATES, 'certificates.pdf', Lang.FR, attachments.get(pdfFile)),
                    new Documentation(DocumentationType.FAQ, 'faq.pdf', Lang.FR, attachments.get(pdfFile))

            ] as Set,
            attachments.get(translations),
            true,
            [
                    new VotationBallotConfig('20180923VP-FED-CH',
                            [
                                    new Documentation(DocumentationType.BALLOT_DOCUMENTATION, "explanations.pdf", Lang.FR, attachments.get(pdfFile))
                            ])
            ],
            [],
            [new Documentation(DocumentationType.HIGHLIGHTED_QUESTION, "question", Lang.FR, attachments.get(pdfFile))],
    )

    pactClient.getVoters(_) >> [
            new Voter([new DomainOfInfluence('CH'), new DomainOfInfluence('GE')], "localPersonId", 1, 1978, 6, 14, null, null)
    ]


  }

  Attachment attachment(String location) {
    def attachment = Mock(Attachment)
    attachment.content >> getClass().getResourceAsStream(location).bytes
    attachment
  }

  def "importOperation should store both operation and voters if there is no other operation stored"() {
    given:
    operationRepository.findAllByProtocolInstanceId('testProtocol') >> []

    OperationHolder savedOperation = null

    when:
    importService.importOperation('testProtocol')

    then:
    1 * operationRepository.save({ savedOperation = it } as OperationHolder)

    savedOperation.canton == 'GE'
    savedOperation.label == 'Votation Populaire'
    savedOperation.defaultLang == Lang.FR
    savedOperation.protocolInstanceId == 'testProtocol'
    savedOperation.modelVersion == 2
    savedOperation.type == OperationType.TEST
    savedOperation.closingDate == closingDate
    savedOperation.openingDate == openingDate
    savedOperation.gracePeriod == 9
    savedOperation.date == operationDate
    savedOperation.elections.size() == 0
    savedOperation.votations.size() == 4
    savedOperation.rawFiles.size() == 5
    savedOperation.rawFiles.name.sort() ==
            ["certificates.pdf", "explanations.pdf", "faq.pdf", "question", "translations.json"]


    JSONAssert.assertEquals(savedOperation.data, getContent("/OperationImportServiceTest.savedOperation.json"), JSONCompareMode.NON_EXTENSIBLE)
    JSONAssert.assertEquals(savedOperation.votations[0].data, getContent("/OperationImportServiceTest.votation0.json"), JSONCompareMode.NON_EXTENSIBLE)
    JSONAssert.assertEquals(savedOperation.votations[1].data, getContent("/OperationImportServiceTest.votation1.json"), JSONCompareMode.NON_EXTENSIBLE)
    JSONAssert.assertEquals(savedOperation.votations[2].data, getContent("/OperationImportServiceTest.votation2.json"), JSONCompareMode.NON_EXTENSIBLE)
    JSONAssert.assertEquals(savedOperation.votations[3].data, getContent("/OperationImportServiceTest.votation3.json"), JSONCompareMode.NON_EXTENSIBLE)

    1 * voterRepository.saveAll({ it.voterId == [1] })
  }

  private String getContent(String location) {
    return new String(getClass().getResourceAsStream(location).bytes, "UTF-8")
  }


  def "importOperation should store no voters if there is same operation in another version"() {
    given:
    operationRepository.findAllByProtocolInstanceId('testProtocol') >> [operationHolder()]

    when:
    importService.importOperation('testProtocol')

    then:
    1 * operationRepository.save(_ as OperationHolder)
    0 * voterRepository.saveAll(_ as Iterable<VoterHolder>)
  }

  OperationHolder operationHolder() {
    OperationHolder operation = new OperationHolder()
    operation
  }

}

/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.service.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration class for the vote receiver service.
 * Used to parse data from the corresponding yaml file.
 */
@Configuration
@ConfigurationProperties(prefix = "vr")
public class VoteReceiverConfigurationProperties {

  private String timezone;
  private long   nbDaysBeforeExpiration;
  private String mockEndpoint;
  private String baseUrl;
  private String voteUrl;
  private int    modelVersion;

  public String getTimezone() {
    return timezone;
  }

  public void setTimezone(String timezone) {
    this.timezone = timezone;
  }

  public long getNbDaysBeforeExpiration() {
    return nbDaysBeforeExpiration;
  }

  public void setNbDaysBeforeExpiration(long nbDaysBeforeExpiration) {
    this.nbDaysBeforeExpiration = nbDaysBeforeExpiration;
  }

  public String getMockEndpoint() {
    return mockEndpoint;
  }

  public void setMockEndpoint(String mockEndpoint) {
    this.mockEndpoint = mockEndpoint;
  }

  public String getBaseUrl() {
    return baseUrl;
  }

  public void setBaseUrl(String baseUrl) {
    this.baseUrl = baseUrl;
  }

  public String getVoteUrl() {
    return voteUrl;
  }

  public void setVoteUrl(String voteUrl) {
    this.voteUrl = voteUrl;
  }

  public int getModelVersion() {
    return modelVersion;
  }

  public void setModelVersion(int modelVersion) {
    this.modelVersion = modelVersion;
  }
}
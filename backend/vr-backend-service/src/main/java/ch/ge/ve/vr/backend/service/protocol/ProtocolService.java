/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.service.protocol;

import ch.ge.ve.protocol.client.ProtocolClient;
import ch.ge.ve.protocol.client.exception.ConfirmationFailedException;
import ch.ge.ve.protocol.core.model.FinalizationCodePart;
import ch.ge.ve.protocol.core.model.ObliviousTransferResponse;
import ch.ge.ve.protocol.model.BallotAndQuery;
import ch.ge.ve.protocol.model.Confirmation;
import ch.ge.ve.protocol.model.ElectionSetForVerification;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.vr.backend.repository.entity.VoterHolder;
import ch.ge.ve.vr.backend.service.voter.VoterService;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Exposes the interfaces to handle the actions linked to the core protocol.
 */
@Service
public class ProtocolService {
  private static final Logger                logger = LoggerFactory.getLogger(ProtocolService.class);
  private final        ProtocolClientFactory protocolClientFactory;
  private final        VoterService          voterService;

  /**
   * Base constructor.
   *
   * @param protocolClientFactory a factory to create {@link ProtocolClient} instances
   * @param voterService          the service to retrieve vote information
   */
  @Autowired
  public ProtocolService(ProtocolClientFactory protocolClientFactory, VoterService voterService) {
    this.protocolClientFactory = protocolClientFactory;
    this.voterService = voterService;
  }

  /**
   * Retrieve the election set linked to the given protocol.
   *
   * @param protocolId the protocol's unique ID
   *
   * @return the associated election set
   */
  public ElectionSetForVerification getElectionSet(String protocolId) {
    return getClient(protocolId).fetchElectionSet();
  }

  /**
   * Retrieve the public parameters linked to a given protocol.
   *
   * @param protocolId the protocol's unique ID
   *
   * @return the associated public parameters
   */
  public PublicParameters getPublicParameters(String protocolId) {
    return getClient(protocolId).fetchPublicParameters();
  }

  /**
   * Retrieve the list of primes used by a given protocol.
   *
   * @param protocolId the protocol's unique ID
   *
   * @return the associated primes
   */
  public List<BigInteger> getPrimes(String protocolId) {
    return getClient(protocolId).fetchPrimes();
  }


  /**
   * Retrieve the list of public keys used by a given protocol.
   *
   * @param protocolId the protocol's unique ID
   *
   * @return the associated public keys
   */
  public List<EncryptionPublicKey> getPublicKeys(String protocolId) {
    List<EncryptionPublicKey> keys = new ArrayList<>(getClient(protocolId).fetchPublicKeyParts().values());
    keys.add(getClient(protocolId).fetchElectionOfficerKey());
    return keys;
  }

  /**
   * Submit an encrypted ballot to the given protocol.
   *
   * @param ballotAndQuery the encrypted ballot
   * @param protocolId     the protocol's unique ID
   * @param voterId        the voter's identifier in the protocol
   *
   * @return An oblivious transfer response to be handled by the caller to build the verification codes
   */
  public List<ObliviousTransferResponse> submitBallot(BallotAndQuery ballotAndQuery,
                                                      String protocolId,
                                                      Integer voterId) {
    List<ObliviousTransferResponse> responses = getClient(protocolId).publishBallot(voterId, ballotAndQuery);
    voterService.markVerificationCodeRevealed(protocolId, voterId);
    return responses;
  }

  /**
   * Submit a confirmation code to validate a vote in the protocol
   *
   * @param confirmation the confirmation code object
   * @param protocolId   the protocol's unique ID
   * @param voterId      the voter's identifier in the protocol
   *
   * @return The finalization code parts to be handled by the caller to build the finalization code
   */
  public List<FinalizationCodePart> submitConfirmationCode(Confirmation confirmation,
                                                           String protocolId,
                                                           Integer voterId) {
    VoterHolder voter = voterService.getVoter(protocolId, voterId);
    voterService.checkVoterConfirmationAllowed(voter);
    try {
      return getClient(protocolId).submitConfirmation(voterId, confirmation);
    } catch (ConfirmationFailedException e) {
      logger.info("Confirmation code failed for protocolId:" + protocolId + " and voter id:" + voterId, e);
      voterService.markVoterConfirmationFailed(voter);
      return Collections.emptyList(); // unreachable by construction
    }
  }

  private ProtocolClient getClient(String protocolId) {
    return protocolClientFactory.client(protocolId);
  }

}

/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.service.exception;

/**
 * Business exception
 */
public class BusinessException extends RuntimeException {
  private final String   messageKey;
  private final String[] parameters;

  public BusinessException() {
    this(null);
  }

  public BusinessException(String messageKey, Throwable cause, String... parameters) {
    super(cause);
    this.messageKey = messageKey;
    this.parameters = parameters;
  }

  public BusinessException(String messageKey, String... parameters) {
    this(messageKey, null, parameters);
  }

  public BusinessException(String messageKey) {
    this(messageKey, null);
  }

  public String getMessageKey() {
    return messageKey;
  }

  public String[] getParameters() {
    return parameters;
  }
}

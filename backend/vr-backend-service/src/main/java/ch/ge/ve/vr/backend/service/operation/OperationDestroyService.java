/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.service.operation;

import ch.ge.ve.vr.backend.repository.OperationRepository;
import ch.ge.ve.vr.backend.repository.VoterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service responible for destroying operation and so on related protocol instances
 */
@Service
public class OperationDestroyService {


  private final OperationRepository operationRepository;
  private final VoterRepository     voterRepository;

  @Autowired
  public OperationDestroyService(OperationRepository operationRepository,
                                 VoterRepository voterRepository) {
    this.operationRepository = operationRepository;
    this.voterRepository = voterRepository;
  }

  @Transactional
  public void destroyInstance(String protocolInstance) {
    /**
     * Delete voters linked to the given protocol instance id
     */
    voterRepository.deleteAllByProtocolInstanceId(protocolInstance);

    /**
     * and then delete all the related operation
     */
    operationRepository.findAllByProtocolInstanceId(protocolInstance)
                       .forEach(operationRepository::delete);
  }


}

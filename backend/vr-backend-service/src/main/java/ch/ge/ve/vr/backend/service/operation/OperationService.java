/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.service.operation;

import ch.ge.ve.chvote.pact.b2b.client.model.OperationType;
import ch.ge.ve.chvote.pact.b2b.client.model.VotingPeriod;
import ch.ge.ve.vr.backend.repository.OperationRepository;
import ch.ge.ve.vr.backend.repository.data.Operation;
import ch.ge.ve.vr.backend.repository.entity.OperationHolder;
import ch.ge.ve.vr.backend.service.JsonDocumentEntityService;
import ch.ge.ve.vr.backend.service.config.VoteReceiverConfigurationProperties;
import ch.ge.ve.vr.backend.service.exception.AccessSecurityException;
import ch.ge.ve.vr.backend.service.model.OperationStatus;
import ch.ge.ve.vr.backend.service.model.OperationSummary;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.Clock;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service to manage operations
 */
@Service
public class OperationService {

  private final OperationRepository                 operationRepository;
  private final Clock                               clock;
  private final VoteReceiverConfigurationProperties config;
  private final JsonDocumentEntityService           jsonDocumentEntityService;

  @Autowired
  public OperationService(OperationRepository operationRepository, Clock clock,
                          VoteReceiverConfigurationProperties config,
                          JsonDocumentEntityService jsonDocumentEntityService) {
    this.operationRepository = operationRepository;
    this.clock = clock;
    this.config = config;
    this.jsonDocumentEntityService = jsonDocumentEntityService;
  }

  /**
   * Fetch all operations by canton and depending mode (test/simulation or real)
   *
   * @param canton:  canton to filter
   * @param forTest: real or test/simulation
   *
   * @return java.util.Map of with ke {@link OperationStatus} and value {@link List} of {@link OperationSummary}
   */
  public Map<OperationStatus, List<OperationSummary>> findOperations(String canton, boolean forTest) {
    final List<OperationHolder> operations;

    LocalDateTime now = LocalDateTime.now(clock);

    if (forTest) {
      operations = operationRepository.findAllForTestMode(
          config.getModelVersion(),
          canton,
          now.minusDays(config.getNbDaysBeforeExpiration()));
    } else {
      operations = operationRepository.findAllForRealMode(
          config.getModelVersion(),
          canton,
          now.minusDays(config.getNbDaysBeforeExpiration()));
    }

    Map<OperationStatus, List<OperationSummary>> operationsByStatus = Map.of(
        OperationStatus.PAST, new ArrayList<>(),
        OperationStatus.IN_PROGRESS, new ArrayList<>(),
        OperationStatus.FUTURE, new ArrayList<>(),
        OperationStatus.IN_TEST, new ArrayList<>());

    for (OperationHolder operation : operations) {
      OperationSummary operationSummary = convert(operation);
      if (OperationType.TEST.equals(operation.getType())) {
        operationsByStatus.get(OperationStatus.IN_TEST).add(operationSummary);

      } else {
        if (operation.getClosingDate() != null && operation.getClosingDate().isBefore(now)) {
          operationsByStatus.get(OperationStatus.PAST).add(operationSummary);
        } else if (operation.getOpeningDate() != null && operation.getOpeningDate().isAfter(now)) {
          operationsByStatus.get(OperationStatus.FUTURE).add(operationSummary);
        } else {
          operationsByStatus.get(OperationStatus.IN_PROGRESS).add(operationSummary);
        }
      }
    }
    return operationsByStatus;
  }

  /**
   * Fetch operation by protocol instance id depending on its type (test or real)
   *
   * @param forTest:            boolean if true means test/simulation, otherwise it's real
   * @param protocolInstanceId: protocol instance id
   *
   * @return Operation
   */
  public Operation getOperation(boolean forTest, String protocolInstanceId) {
    OperationHolder operation = getOperation(protocolInstanceId);
    checkOperationAccess(forTest, protocolInstanceId);
    return jsonDocumentEntityService.convert(operation);
  }

  public Operation getOperationWithoutOperationCheckAccess(String protocolInstanceId) {
    return jsonDocumentEntityService.convert(getOperation(protocolInstanceId));
  }

  public OperationHolder getOperation(String protocolInstanceId) {
    return operationRepository
        .findByProtocolInstanceIdAndModelVersion(protocolInstanceId, config.getModelVersion())
        .orElseThrow(() -> new EntityNotFoundException(
            String.format("cannot find operation for protocol instance ID %s", protocolInstanceId)));
  }

  private OperationSummary convert(OperationHolder operation) {
    return new OperationSummary(
        operation.getLabel(),
        URLEncoder.encode(operation.getProtocolInstanceId(), StandardCharsets.UTF_8),
        operation.getDefaultLang(),
        operation.getType() == OperationType.TEST ? null :
            new VotingPeriod(operation.getOpeningDate(), operation.getClosingDate(), operation.getGracePeriod()),
        operation.getDate()
    );
  }

  /**
   * Check operation access rights
   *
   * @param forTest            whether it's a test operation or a real operation
   * @param protocolInstanceId the protocol instance ID
   *
   * @return the operation's ID
   *
   * @throws EntityNotFoundException if no corresponding operation is found
   */
  public Integer checkOperationAccess(boolean forTest, String protocolInstanceId) {
    OperationHolder operation = getOperation(protocolInstanceId);
    checkOperationAccess(forTest, operation);
    return operation.getId();
  }

  private void checkOperationAccess(boolean forTest, OperationHolder operationHolder) {
    if (OperationType.REAL.equals(operationHolder.getType()) == forTest) {
      throw AccessSecurityException.wrongAccessMode(operationHolder.getType(), forTest);
    }
  }

}

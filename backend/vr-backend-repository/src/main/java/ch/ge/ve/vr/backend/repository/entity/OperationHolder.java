/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.repository.entity;

import ch.ge.ve.chvote.pact.b2b.client.model.Lang;
import ch.ge.ve.chvote.pact.b2b.client.model.OperationType;
import ch.ge.ve.vr.backend.repository.data.Operation;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Entity containing an operation instance as a JSON document
 */
@Entity
@Table(name = "VR_T_OPERATION")
public class OperationHolder implements Data<Operation> {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "OPE_N_ID", nullable = false)
  private Integer id;

  @Column(name = "OPE_C_CANTON", nullable = false)
  private String canton;

  @Column(name = "OPE_C_LABEL")
  private String label;

  @Column(name = "OPE_C_DEFAULT_LANG", nullable = false)
  @Enumerated(EnumType.STRING)
  private Lang defaultLang;

  @Column(name = "OPE_C_PROTOCOL_INSTANCE_ID", nullable = false)
  private String protocolInstanceId;

  @Column(name = "OPE_N_MODEL_VERSION", nullable = false)
  private int modelVersion;

  @Column(name = "OPE_C_TYPE", nullable = false)
  @Enumerated(EnumType.STRING)
  private OperationType type;

  @Column(name = "OPE_D_CLOSING_DATE")
  private LocalDateTime closingDate;

  @Column(name = "OPE_D_OPENING_DATE")
  private LocalDateTime openingDate;

  @Column(name = "OPE_N_GRACE_PERIOD")
  private Integer gracePeriod;

  @Column(name = "OPE_D_OPERATION_DATE")
  private LocalDateTime date;

  @Column(name = "OPE_D_TRANSLATION_FILE_INDEX")
  private int translationFileIndex;

  @Lob
  @Column(name = "OPE_X_DATA")
  private String data;

  @OneToMany(mappedBy = "operation", cascade = CascadeType.ALL)
  private Set<RawFile> rawFiles;

  @OneToMany(mappedBy = "operation", cascade = CascadeType.ALL)
  private List<ElectionHolder> elections;

  @OneToMany(mappedBy = "operation", cascade = CascadeType.ALL)
  private List<VotationHolder> votations;

  @OneToMany(mappedBy = "operation", cascade = CascadeType.ALL)
  private List<FinalizationLog> finalizationLogs;

  @Override
  public Class<Operation> getDataClass() {
    return Operation.class;
  }

  @Override
  public String getData() {
    return data;
  }

  @Override
  public void setData(String data) {
    this.data = data;
  }

  public String getCanton() {
    return canton;
  }

  public void setCanton(String canton) {
    this.canton = canton;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public Lang getDefaultLang() {
    return defaultLang;
  }

  public void setDefaultLang(Lang defaultLang) {
    this.defaultLang = defaultLang;
  }

  public String getProtocolInstanceId() {
    return protocolInstanceId;
  }

  public void setProtocolInstanceId(String protocolInstanceId) {
    this.protocolInstanceId = protocolInstanceId;
  }

  public OperationType getType() {
    return type;
  }

  public void setType(OperationType type) {
    this.type = type;
  }

  public LocalDateTime getClosingDate() {
    return closingDate;
  }

  public void setClosingDate(LocalDateTime closingDate) {
    this.closingDate = closingDate;
  }

  public LocalDateTime getOpeningDate() {
    return openingDate;
  }

  public void setOpeningDate(LocalDateTime openingDate) {
    this.openingDate = openingDate;
  }

  public Integer getGracePeriod() {
    return gracePeriod;
  }

  public void setGracePeriod(Integer gracePeriod) {
    this.gracePeriod = gracePeriod;
  }

  public LocalDateTime getDate() {
    return date;
  }

  public void setDate(LocalDateTime date) {
    this.date = date;
  }

  public List<ElectionHolder> getElections() {
    return elections;
  }

  public void setElections(List<ElectionHolder> elections) {
    this.elections = elections;
  }

  public List<VotationHolder> getVotations() {
    return votations;
  }

  public void setVotations(List<VotationHolder> votations) {
    this.votations = votations;
  }

  public Set<RawFile> getRawFiles() {
    return rawFiles;
  }

  public void setRawFiles(Set<RawFile> rawFiles) {
    this.rawFiles = rawFiles;
  }

  /**
   * Get the frontend model version number
   */
  public int getModelVersion() {
    return modelVersion;
  }


  /**
   * Set the frontend model version number as defined in application's configuration
   */
  public void setModelVersion(int modelVersion) {
    this.modelVersion = modelVersion;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public int getTranslationFileIndex() {
    return translationFileIndex;
  }

  public void setTranslationFileIndex(int translationFileIndex) {
    this.translationFileIndex = translationFileIndex;
  }

  public List<FinalizationLog> getFinalizationLogs() {
    return finalizationLogs;
  }

  public void setFinalizationLogs(List<FinalizationLog> finalizationLogs) {
    this.finalizationLogs = finalizationLogs;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OperationHolder that = (OperationHolder) o;
    return Objects.equals(protocolInstanceId, that.protocolInstanceId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(protocolInstanceId);
  }
}

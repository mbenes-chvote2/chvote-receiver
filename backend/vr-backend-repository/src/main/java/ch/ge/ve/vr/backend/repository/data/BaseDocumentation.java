/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.repository.data;

import ch.ge.ve.chvote.pact.b2b.client.model.Lang;

/**
 * Base class for documentation data.
 */
public abstract class BaseDocumentation {
  private final String label;
  private final Lang   lang;
  private final int    fileIndex;

  BaseDocumentation(String label, Lang lang, int fileIndex) {
    this.label = label;
    this.lang = lang;
    this.fileIndex = fileIndex;
  }

  public String getLabel() {
    return label;
  }

  public Lang getLang() {
    return lang;
  }

  public int getFileIndex() {
    return fileIndex;
  }
}
